package sbd.biblioteka;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.*;
import java.awt.event.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.swing.SpinnerDateModel;
import javax.swing.table.AbstractTableModel;
import static javax.swing.JOptionPane.showMessageDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import sbd.biblioteka.domain.Address;
import sbd.biblioteka.domain.Author;
import sbd.biblioteka.domain.Employee;
import sbd.biblioteka.domain.Book;
import sbd.biblioteka.domain.Branch;
import sbd.biblioteka.domain.Client;
import sbd.biblioteka.domain.Genre;
import sbd.biblioteka.domain.Library;
import sbd.biblioteka.domain.LibraryCard;
import sbd.biblioteka.domain.Position;
import sbd.biblioteka.domain.ReadingRoom;
import sbd.biblioteka.domain.Registry;
import sbd.biblioteka.domain.Rental;

public class Biblioteka extends JFrame {

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;

    JButton libraryButton, employeesButton, branchesButton, clientsButton,
            rentalsButton, booksButton, readingRoomsButton, libraryCardButton,
            genresButton, authorsButton, registersButton, positionsButton;

    JFrame infoFrame;
    JFrame branchesFrame;
    JFrame employeesFrame;
    JFrame clientsFrame;
    JFrame rentalFrame;
    JFrame booksFrame;
    JFrame readingRoomsFrame;
    JFrame libraryCardFrame;
    JFrame authorsFrame;
    JFrame genresFrame;
    JFrame registersFrame;
    JFrame positionsFrame;

    public static boolean isInfoWindowOpen = false;
    public static boolean isBranchesWindowOpen = false;
    public static boolean isEmployeesWindowOpen = false;
    public static boolean isClientsWindowOpen = false;
    public static boolean isRentalsWindowOpen = false;
    public static boolean isBooksWindowOpen = false;
    public static boolean isReadingRoomsWindowOpen = false;
    public static boolean isLibraryCardWindowOpen = false;
    public static boolean isAuthorsWindowOpen = false;
    public static boolean isGenresWindowOpen = false;
    public static boolean isRegistersWindowOpen = false;
    public static boolean isPositionsWindowOpen = false;

    public boolean isNumeric(String str) {
        return str.matches("^\\d+$");
    }

    public Biblioteka() {

        entityManagerFactory = Persistence.createEntityManagerFactory("myDb");
        entityManager = entityManagerFactory.createEntityManager();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setSize(400, 400);

        Toolkit tk = Toolkit.getDefaultToolkit();

        Dimension dim = tk.getScreenSize();

        int xPos = (dim.width / 2) - (this.getWidth() / 2);
        int yPos = (dim.height / 2) - (this.getHeight() / 2);

        this.setLocation(xPos, yPos);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setTitle("Zarządzanie biblioteką");

        JPanel thePanel = new JPanel();
        thePanel.setBorder(BorderFactory.createTitledBorder("Zarządzaj"));
        thePanel.setLayout(new GridLayout(4, 3, 5, 5));

        libraryButton = new JButton("Informacje o instytucji");
        employeesButton = new JButton("Pracownicy");
        branchesButton = new JButton("Oddziały");
        clientsButton = new JButton("Klienci");
        rentalsButton = new JButton("Wypożyczenia");
        booksButton = new JButton("Ksiązki");
        readingRoomsButton = new JButton("Czytelnie");
        libraryCardButton = new JButton("Karty biblioteczne");
        authorsButton = new JButton("Autorzy");
        genresButton = new JButton("Gatunki");
        registersButton = new JButton("Rejestry");
        positionsButton = new JButton("Stanowiska");

        ListenForButton lForButton = new ListenForButton();

        libraryButton.addActionListener(lForButton);
        employeesButton.addActionListener(lForButton);
        branchesButton.addActionListener(lForButton);
        clientsButton.addActionListener(lForButton);
        rentalsButton.addActionListener(lForButton);
        booksButton.addActionListener(lForButton);
        readingRoomsButton.addActionListener(lForButton);
        libraryCardButton.addActionListener(lForButton);
        authorsButton.addActionListener(lForButton);
        genresButton.addActionListener(lForButton);
        registersButton.addActionListener(lForButton);
        positionsButton.addActionListener(lForButton);

        thePanel.add(libraryButton);
        thePanel.add(branchesButton);
        thePanel.add(employeesButton);
        thePanel.add(clientsButton);
        thePanel.add(positionsButton);
        thePanel.add(rentalsButton);
        thePanel.add(booksButton);
        thePanel.add(readingRoomsButton);
        thePanel.add(libraryCardButton);
        thePanel.add(authorsButton);
        thePanel.add(genresButton);
        thePanel.add(registersButton);

        this.add(thePanel);

        this.setVisible(true);

    }

    private class ListenForButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == libraryButton) {
                if (!Biblioteka.isInfoWindowOpen) {
                    openLibraryInfo();
                }
            } else if (e.getSource() == branchesButton) {
                if (!Biblioteka.isBranchesWindowOpen) {
                    openBranches();
                }
            } else if (e.getSource() == employeesButton) {
                if (!Biblioteka.isEmployeesWindowOpen) {
                    openEmployees();
                }
            } else if (e.getSource() == clientsButton) {
                if (!Biblioteka.isClientsWindowOpen) {
                    openClients();
                }
            } else if (e.getSource() == rentalsButton) {
                if (!Biblioteka.isRentalsWindowOpen) {
                    openRentals();
                }

            } else if (e.getSource() == libraryCardButton) {
                if (!Biblioteka.isLibraryCardWindowOpen) {
                    openLibraryCard();
                }

            } else if (e.getSource() == booksButton) {
                if (!Biblioteka.isBooksWindowOpen) {
                    openBooks();
                }
            } else if (e.getSource() == authorsButton) {
                if (!Biblioteka.isAuthorsWindowOpen) {
                    openAuthors();
                }
            } else if (e.getSource() == genresButton) {
                if (!Biblioteka.isGenresWindowOpen) {
                    openGenres();
                }
            } else if (e.getSource() == readingRoomsButton) {
                if (!Biblioteka.isReadingRoomsWindowOpen) {
                    openReadingRooms();
                }
            } else if (e.getSource() == registersButton) {
                if (!Biblioteka.isRegistersWindowOpen) {
                    openRegisters();
                }
            } else if (e.getSource() == positionsButton) {
                if (!Biblioteka.isPositionsWindowOpen) {
                    openPositions();
                }
            }
        }
    }

    private class ListenForWindow implements WindowListener {

        @Override
        public void windowOpened(WindowEvent e) {
            if (e.getWindow() == rentalFrame) {
                Biblioteka.isRentalsWindowOpen = true;
            } else if (e.getWindow() == branchesFrame) {
                Biblioteka.isBranchesWindowOpen = true;
            } else if (e.getWindow() == employeesFrame) {
                Biblioteka.isEmployeesWindowOpen = true;
            } else if (e.getWindow() == clientsFrame) {
                Biblioteka.isClientsWindowOpen = true;
            } else if (e.getWindow() == infoFrame) {
                Biblioteka.isInfoWindowOpen = true;
            } else if (e.getWindow() == libraryCardFrame) {
                Biblioteka.isLibraryCardWindowOpen = true;
            } else if (e.getWindow() == booksFrame) {
                Biblioteka.isBooksWindowOpen = true;
            } else if (e.getWindow() == authorsFrame) {
                Biblioteka.isAuthorsWindowOpen = true;
            } else if (e.getWindow() == genresFrame) {
                Biblioteka.isGenresWindowOpen = true;
            } else if (e.getWindow() == readingRoomsFrame) {
                Biblioteka.isReadingRoomsWindowOpen = true;
            } else if (e.getWindow() == registersFrame) {
                Biblioteka.isRegistersWindowOpen = true;
            }

        }

        @Override
        public void windowClosing(WindowEvent e) {

        }

        @Override
        public void windowClosed(WindowEvent e) {
            if (e.getWindow() == rentalFrame) {
                isRentalsWindowOpen = false;
            } else if (e.getWindow() == branchesFrame) {
                isBranchesWindowOpen = false;
            } else if (e.getWindow() == employeesFrame) {
                isEmployeesWindowOpen = false;
            } else if (e.getWindow() == clientsFrame) {
                isClientsWindowOpen = false;
            } else if (e.getWindow() == infoFrame) {
                isInfoWindowOpen = false;
            } else if (e.getWindow() == libraryCardFrame) {
                isLibraryCardWindowOpen = false;
            } else if (e.getWindow() == booksFrame) {
                isBooksWindowOpen = false;
            } else if (e.getWindow() == authorsFrame) {
                isAuthorsWindowOpen = false;
            } else if (e.getWindow() == genresFrame) {
                isGenresWindowOpen = false;
            } else if (e.getWindow() == readingRoomsFrame) {
                isReadingRoomsWindowOpen = false;
            } else if (e.getWindow() == registersFrame) {
                isRegistersWindowOpen = false;
            }
        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {

        }

        @Override
        public void windowActivated(WindowEvent e) {

        }

        @Override
        public void windowDeactivated(WindowEvent e) {

        }

    }

    public class LibraryInfo extends JFrame {

        JPanel panel, openHoursPanel, namePanel;
        JLabel openTimeLabel, openHoursLabel;
        JButton changeOpenTimeButton;
        

        public LibraryInfo() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    infoFrame = new JFrame("Informacje");

                    infoFrame.setSize(190, 180);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (infoFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (infoFrame.getHeight() / 2);

                    infoFrame.setLocation(xPos, yPos);

                    infoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    panel = new JPanel();
                    openHoursPanel = new JPanel();
                    namePanel = new JPanel();
                    
                    ListenForButton lfb = new ListenForButton();
                    changeOpenTimeButton = new JButton("Zmień godziny");
                    changeOpenTimeButton.addActionListener(lfb);
                   
                    namePanel.add(new JLabel("Nazwa: "));
                    namePanel.add(new JLabel("Biblioteka"));
                    
                    openHoursLabel = new JLabel();
                    refresh();
                    
                    panel.setLayout(new BorderLayout());
                          
                    openHoursPanel.setBorder(BorderFactory.createTitledBorder("Godziny otwarcia"));
                    openHoursPanel.add(openHoursLabel);

                    panel.add(namePanel, BorderLayout.PAGE_START);
                    panel.add(openHoursPanel, BorderLayout.CENTER);
                    panel.add(changeOpenTimeButton, BorderLayout.PAGE_END);

                    infoFrame.add(panel);

                    infoFrame.setVisible(true);
                    infoFrame.setResizable(false);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    infoFrame.addWindowListener(listenForWindow);
                }
            });

        }
        
        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == changeOpenTimeButton) {
                    changeOpenTime();
                } 

            }
        }
        
        public void refresh(){
            entityManager.getTransaction().begin();

            Query q1 = entityManager.createQuery("SELECT l FROM Library l");

            Library library = (Library) q1.getSingleResult();

            entityManager.getTransaction().commit();
            
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            
            Calendar c = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            
            c.setTime(library.getOpenTime());
            c2.setTime(library.getCloseTime());
            String s = sdf.format(c.getTime());
            String s2 = sdf.format(c2.getTime());
            
            
            openHoursLabel.setText(s + " - " + s2);
        }
        
        public void changeOpenTime(){
  
                
                entityManager.getTransaction().begin();

                Query q1 = entityManager.createQuery("SELECT l FROM Library l");

                Library l = (Library) q1.getSingleResult();

                entityManager.getTransaction().commit();


                JPanel panel = new JPanel();

                JLabel openTimeLabel = new JLabel("Godzina otwarcia: ");
                JSpinner openTimeSpinner = new JSpinner(new SpinnerDateModel(l.getOpenTime(), null, null, Calendar.HOUR));
                JSpinner.DateEditor de = new JSpinner.DateEditor(openTimeSpinner, "HH:mm");
                openTimeSpinner.setEditor(de);
                
                JLabel closeTimeLabel = new JLabel("Godzina zamknięcia: ");
                JSpinner closeTimeSpinner = new JSpinner(new SpinnerDateModel(l.getCloseTime(), null, null, Calendar.HOUR));
                JSpinner.DateEditor de2 = new JSpinner.DateEditor(closeTimeSpinner, "HH:mm");
                closeTimeSpinner.setEditor(de2);

                panel.setLayout(new GridLayout(3, 2));

                panel.add(openTimeLabel);
                panel.add(openTimeSpinner);
                openTimeSpinner.getModel().setValue(l.getOpenTime());
                
                panel.add(closeTimeLabel);
                panel.add(closeTimeSpinner);
                closeTimeSpinner.getModel().setValue(l.getCloseTime());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Zmień godziny",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {
                            
                            java.util.Date openD = (java.util.Date)openTimeSpinner.getModel().getValue();
                            java.util.Date closeD = (java.util.Date)closeTimeSpinner.getModel().getValue();
                            
                            Time openT = new Time(openD.getTime());
                            Time closeT = new Time(closeD.getTime());
                            
                            l.setOpenTime(openT);
                            l.setCloseTime(closeT);

                            entityManager.getTransaction().begin();
                            entityManager.merge(l);
                            entityManager.getTransaction().commit();
                            
                            refresh();

                        }
                    }
                });
            }
        }
    

    public class Branches extends JFrame {

        JPanel panel, searchPanel;
        JPanel crudPanel;
        JButton addBranchButton, editBranchButton, deleteBranchButton;
        JButton searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        BranchTableModel btm;
        JTable table;

        public Branches() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    branchesFrame = new JFrame("Oddziały");

                    btm = new BranchTableModel();

                    table = new JTable(btm);
                    table.setAutoCreateRowSorter(true);

                    branchesFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (branchesFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (branchesFrame.getHeight() / 2);

                    branchesFrame.setLocation(xPos, yPos);

                    branchesFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addBranchButton = new JButton("Dodaj oddział");
                    editBranchButton = new JButton("Edytuj oddział");
                    deleteBranchButton = new JButton("Usuń oddział");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);
                    searchField.setBackground(Color.WHITE);

                    List<String> searchFiltersList = new ArrayList<String>();

                    String[] tmp = btm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    ListenForButton lfb = new ListenForButton();

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    searchButton = new JButton("Wyszukaj");

                    addBranchButton.addActionListener(lfb);
                    editBranchButton.addActionListener(lfb);
                    deleteBranchButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Oddziały"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addBranchButton);
                    crudPanel.add(editBranchButton);
                    crudPanel.add(deleteBranchButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    branchesFrame.add(panel, BorderLayout.CENTER);
                    branchesFrame.add(crudPanel, BorderLayout.PAGE_START);
                    branchesFrame.add(searchPanel, BorderLayout.PAGE_END);

                    branchesFrame.setVisible(true);
                    branchesFrame.setResizable(false);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    branchesFrame.addWindowListener(listenForWindow);
                }
            });

        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addBranchButton) {
                    addBranch();

                } else if (e.getSource() == editBranchButton) {
                    if (table.getSelectedRowCount() == 1) {
                        editBranch(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteBranchButton) {
                    deleteBranch(table.getSelectedRows());

                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        btm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    btm.refresh();
                }

            }
        }

        private void addBranch() {

            JPanel panel = new JPanel();

            JLabel nameLabel = new JLabel("Nazwa: ");
            JTextField nameTextField = new JTextField("");

            JLabel cityLabel = new JLabel("Miasto: ");
            JTextField cityTextField = new JTextField("");

            JLabel strettLabel = new JLabel("Ulica: ");
            JTextField streetTextField = new JTextField("");

            JLabel streetNumberLabel = new JLabel("Nr budynku: ");
            JTextField streetNumberTextField = new JTextField("");

            panel.setLayout(new GridLayout(6, 2));

            panel.add(nameLabel);
            panel.add(nameTextField);

            panel.add(cityLabel);
            panel.add(cityTextField);

            panel.add(strettLabel);
            panel.add(streetTextField);

            panel.add(streetNumberLabel);
            panel.add(streetNumberTextField);

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowy oddział",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        if (nameTextField.getText().isEmpty()) {
                            showMessageDialog(null, "Pole nazwa może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        Branch b = new Branch();
                        b.setName(nameTextField.getText());

                        Address a = new Address();
                        a.setCity(cityTextField.getText());
                        a.setStreet(streetTextField.getText());
                        a.setStreetNumber(streetNumberTextField.getText());

                        b.setAddress(a);

                        entityManager.getTransaction().begin();
                        entityManager.persist(a);
                        entityManager.persist(b);
                        entityManager.getTransaction().commit();

                        btm.refresh();

                    }
                }
            });
        }

        private void editBranch(int idx) {

            Branch b = btm.branchList.get(idx);

            Address a = btm.branchList.get(idx).getAddress();

            JPanel panel = new JPanel();

            JLabel nameLabel = new JLabel("Nazwa: ");
            JTextField nameTextField = new JTextField("");

            JLabel cityLabel = new JLabel("Miasto: ");
            JTextField cityTextField = new JTextField("");

            JLabel strettLabel = new JLabel("Ulica: ");
            JTextField streetTextField = new JTextField("");

            JLabel streetNumberLabel = new JLabel("Nr budynku: ");
            JTextField streetNumberTextField = new JTextField("");

            panel.setLayout(new GridLayout(6, 2));

            panel.add(nameLabel);
            panel.add(nameTextField);
            nameTextField.setText(b.getName());

            panel.add(cityLabel);
            panel.add(cityTextField);
            cityTextField.setText(b.getAddress().getCity());

            panel.add(strettLabel);
            panel.add(streetTextField);
            streetTextField.setText(b.getAddress().getStreet());

            panel.add(streetNumberLabel);
            panel.add(streetNumberTextField);
            streetNumberTextField.setText(b.getAddress().getStreetNumber());

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        if (nameTextField.getText().isEmpty()) {
                            showMessageDialog(null, "Pole nazwa może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        b.setName(nameTextField.getText());

                        a.setCity(cityTextField.getText());
                        a.setStreet(streetTextField.getText());
                        a.setStreetNumber(streetNumberTextField.getText());

                        b.setAddress(a);

                        entityManager.getTransaction().begin();
                        entityManager.merge(a);
                        entityManager.merge(b);
                        entityManager.getTransaction().commit();

                        btm.refresh();

                    }
                }
            });
        }

        private void deleteBranch(int[] idx) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                entityManager.getTransaction().begin();
                for (int id : idx) {
                    Branch c = btm.branchList.get(id);
                    entityManager.remove(c);
                }
                entityManager.getTransaction().commit();
                btm.refresh();
            }
        }

        private void searchByColumn(String param, String column) {
            String query = new String();
            if (column.compareTo("id") == 0) {
                if (isNumeric(param)) {
                    btm.findByID(Integer.parseInt(param));
                } else {
                    showMessageDialog(null, "'" + param + "' " + "nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                }
                return;
            } else if (column.compareTo("Nazwa") == 0) {
                query = "FROM Branch b WHERE UPPER(b.name) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Miasto") == 0) {
                query = "FROM Branch b WHERE UPPER(b.address.city) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Ulica") == 0) {
                query = "FROM Branch b WHERE UPPER(b.address.street) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Nr budynku") == 0) {
                query = "FROM Branch b WHERE b.address.streetNumber=:parameter)";
            }

            btm.findByQuery(query, param);
        }
    }

    public class Clients extends JFrame {

        JPanel panel;
        JPanel crudPanel;
        JPanel buttonsPanel;
        JButton addClientButton, editClientButton, deleteClientButton;
        JButton showLibraryCardButton, searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        PersonTableModel ptm;

        public Clients() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    clientsFrame = new JFrame("Klienci");

                    ptm = new PersonTableModel();
                    table = new JTable(ptm);
                    table.setAutoCreateRowSorter(true);

                    clientsFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (clientsFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (clientsFrame.getHeight() / 2);

                    clientsFrame.setLocation(xPos, yPos);

                    clientsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addClientButton = new JButton("Dodaj klienta");
                    editClientButton = new JButton("Edytuj klienta");
                    deleteClientButton = new JButton("Usuń klienta");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = ptm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    showLibraryCardButton = new JButton("Pokaż wypożyczenia");
                    searchButton = new JButton("Wyszukaj");

                    ListenForButton lfb = new ListenForButton();

                    addClientButton.addActionListener(lfb);
                    editClientButton.addActionListener(lfb);
                    deleteClientButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    //showLibraryCardButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);

                    panel = new JPanel();
                    panel.setLayout(new BorderLayout());
                    crudPanel = new JPanel();
                    Box box = Box.createHorizontalBox();
                    box.add(searchField);
                    box.add(searchButton);

                    searchField.setBackground(Color.WHITE);
                    buttonsPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Klienci"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addClientButton);
                    crudPanel.add(editClientButton);
                    crudPanel.add(deleteClientButton);
                    //crudPanel.add(showLibraryCardButton);
                    crudPanel.add(refreshButton);

                    buttonsPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    buttonsPanel.add(searchByComboBox);
                    buttonsPanel.add(box);

                    clientsFrame.add(panel, BorderLayout.CENTER);
                    clientsFrame.add(crudPanel, BorderLayout.PAGE_START);
                    clientsFrame.add(buttonsPanel, BorderLayout.PAGE_END);

                    clientsFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    clientsFrame.addWindowListener(listenForWindow);
                }
            });

        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addClientButton) {
                    addClient();

                } else if (e.getSource() == editClientButton) {
                    if (table.getSelectedRowCount() == 1) {
                        editClient(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteClientButton) {
                    deleteClient(table.getSelectedRows());

                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        ptm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }

                } else if (e.getSource() == showLibraryCardButton) {
                    if (table.getSelectedRowCount() == 1) {
                        showLibraryCard(table.getSelectedRow());
                    }
                } else if (e.getSource() == refreshButton) {
                    ptm.refresh();
                }
            }

        }

        private void addClient() {

            JPanel panel = new JPanel();

            JLabel nameLabel = new JLabel("Imię: ");
            JTextField nameTextField = new JTextField("");

            JLabel surnnameLabel = new JLabel("Nazwisko: ");
            JTextField surnameTextField = new JTextField("");

            JLabel emailLabel = new JLabel("Email: ");
            JTextField emailTextField = new JTextField("");

            JLabel cityLabel = new JLabel("Miasto: ");
            JTextField cityTextField = new JTextField("");

            JLabel strettLabel = new JLabel("Ulica: ");
            JTextField streetTextField = new JTextField("");

            JLabel streetNumberLabel = new JLabel("Nr budynku: ");
            JTextField streetNumberTextField = new JTextField("");

            panel.setLayout(new GridLayout(6, 2));

            panel.add(nameLabel);
            panel.add(nameTextField);

            panel.add(surnnameLabel);
            panel.add(surnameTextField);

            panel.add(emailLabel);
            panel.add(emailTextField);

            panel.add(cityLabel);
            panel.add(cityTextField);

            panel.add(strettLabel);
            panel.add(streetTextField);

            panel.add(streetNumberLabel);
            panel.add(streetNumberTextField);

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowego klienta",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty()) {
                            showMessageDialog(null, "Pole imie/nazwisko nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        Client c = new Client();
                        c.setName(nameTextField.getText());
                        c.setSurname(surnameTextField.getText());
                        c.setEmail(emailTextField.getText());

                        Address a = new Address();
                        a.setCity(cityTextField.getText());
                        a.setStreet(streetTextField.getText());
                        a.setStreetNumber(streetNumberTextField.getText());

                        c.setAddress(a);

                        entityManager.getTransaction().begin();
                        entityManager.persist(a);
                        entityManager.persist(c);
                        entityManager.getTransaction().commit();

                        ptm.refresh();

                    }
                }
            });
        }

        private void editClient(int idx) {

            Client c = ptm.clientList.get(idx);

            Address a = ptm.clientList.get(idx).getAddress();

            JPanel panel = new JPanel();

            JLabel nameLabel = new JLabel("Imię: ");
            JTextField nameTextField = new JTextField("");

            JLabel surnnameLabel = new JLabel("Nazwisko: ");
            JTextField surnameTextField = new JTextField("");

            JLabel emailLabel = new JLabel("Email: ");
            JTextField emailTextField = new JTextField("");

            JLabel cityLabel = new JLabel("Miasto: ");
            JTextField cityTextField = new JTextField("");

            JLabel strettLabel = new JLabel("Ulica: ");
            JTextField streetTextField = new JTextField("");

            JLabel streetNumberLabel = new JLabel("Nr budynku: ");
            JTextField streetNumberTextField = new JTextField("");

            panel.setLayout(new GridLayout(6, 2));

            panel.add(nameLabel);
            panel.add(nameTextField);
            nameTextField.setText(c.getName());

            panel.add(surnnameLabel);
            panel.add(surnameTextField);
            surnameTextField.setText(c.getSurname());

            panel.add(emailLabel);
            panel.add(emailTextField);
            emailTextField.setText(c.getEmail());

            panel.add(cityLabel);
            panel.add(cityTextField);
            cityTextField.setText(c.getAddress().getCity());

            panel.add(strettLabel);
            panel.add(streetTextField);
            streetTextField.setText(c.getAddress().getStreet());

            panel.add(streetNumberLabel);
            panel.add(streetNumberTextField);
            streetNumberTextField.setText(c.getAddress().getStreetNumber());

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty()) {
                            showMessageDialog(null, "Pole imie/nazwisko nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        c.setName(nameTextField.getText());
                        c.setSurname(surnameTextField.getText());
                        c.setEmail(emailTextField.getText());

                        a.setCity(cityTextField.getText());
                        a.setStreet(streetTextField.getText());
                        a.setStreetNumber(streetNumberTextField.getText());

                        c.setAddress(a);

                        entityManager.getTransaction().begin();
                        entityManager.merge(a);
                        entityManager.merge(c);
                        entityManager.getTransaction().commit();

                        ptm.refresh();

                    }
                }
            });
        }

        private void deleteClient(int[] idx) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                entityManager.getTransaction().begin();
                for (int id : idx) {
                    Client c = ptm.clientList.get(id);
                    entityManager.remove(c);
                }
                entityManager.getTransaction().commit();
                ptm.refresh();
            }
        }

        private void showLibraryCard(int idx) {

            Client c = ptm.clientList.get(idx);
            //LibraryCard lc = LibraryCard.findByClientID(entityManager, c);
            System.err.println("TEST3");
            //rentalFrame = new Rentals(lc);
            System.err.println("TEST4");
        }

        private void searchByColumn(String param, String column) {
            String query = new String();
            if (column.compareTo("id") == 0) {
                if (isNumeric(param)) {
                    ptm.findByID(Integer.parseInt(param));
                } else {
                    showMessageDialog(null, "id musi być liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                }
                return;
            } else if (column.compareTo("Imię") == 0) {
                query = "FROM Client c WHERE UPPER(c.name) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Nazwisko") == 0) {
                query = "FROM Client c WHERE UPPER(c.surname) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Email") == 0) {
                query = "FROM Client c WHERE UPPER(c.email) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Miasto") == 0) {
                query = "FROM Client c WHERE UPPER(c.address.city) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Ulica") == 0) {
                query = "FROM Client c WHERE UPPER(c.address.street) LIKE UPPER(:parameter)";
            } else if (column.compareTo("Nr budynku") == 0) {
                query = "FROM Client c WHERE c.address.streetNumber=:parameter)";
            }

            ptm.findByQuery(query, param);
        }
    }

    public class Employees extends JFrame {

        JPanel panel, searchPanel;
        JPanel crudPanel;
        JButton addEmployeeButton, editEmployeeButton, deleteEmployeeButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        EmployeeTableModel etm;

        public Employees() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    employeesFrame = new JFrame("Pracownicy");

                    etm = new EmployeeTableModel();

                    table = new JTable(etm);
                    table.setAutoCreateRowSorter(true);

                    employeesFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (employeesFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (employeesFrame.getHeight() / 2);

                    employeesFrame.setLocation(xPos, yPos);

                    employeesFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addEmployeeButton = new JButton("Dodaj pracownika");
                    editEmployeeButton = new JButton("Edytuj pracownika");
                    deleteEmployeeButton = new JButton("Usuń pracownika");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = etm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addEmployeeButton.addActionListener(lfb);
                    editEmployeeButton.addActionListener(lfb);
                    deleteEmployeeButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Pracownicy"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addEmployeeButton);
                    crudPanel.add(editEmployeeButton);
                    crudPanel.add(deleteEmployeeButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    employeesFrame.add(panel, BorderLayout.CENTER);
                    employeesFrame.add(crudPanel, BorderLayout.PAGE_START);
                    employeesFrame.add(searchPanel, BorderLayout.PAGE_END);

                    employeesFrame.setVisible(true);
                    employeesFrame.setResizable(false);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    employeesFrame.addWindowListener(listenForWindow);
                }
            });

        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addEmployeeButton) {
                    addEmployee();

                } else if (e.getSource() == editEmployeeButton) {
                    if (table.getSelectedRowCount() == 1) {
                        editEmployee(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteEmployeeButton) {
                    if (table.getSelectedRowCount() > 0) {
                        deleteEmployee(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        etm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    etm.refresh();
                }

            }

            private void addEmployee() {

                List<Branch> branches;
                List<Position> positions;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from Branch");
                branches = new ArrayList<>(q.getResultList());

                q = entityManager.createQuery("from Position");
                positions = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Imię: ");
                JTextField nameTextField = new JTextField("");

                JLabel surnnameLabel = new JLabel("Nazwisko: ");
                JTextField surnameTextField = new JTextField("");

                JLabel positionLabel = new JLabel("Stanowisko: ");
                JComboBox positionComboBox = new JComboBox<>(positions.toArray());

                JLabel branchLabel = new JLabel("Oddział: ");
                JComboBox branchComboBox = new JComboBox<>(branches.toArray());

                panel.setLayout(new GridLayout(4, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);

                panel.add(surnnameLabel);
                panel.add(surnameTextField);

                panel.add(positionLabel);
                panel.add(positionComboBox);

                panel.add(branchLabel);
                panel.add(branchComboBox);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowego pracownika",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Employee e = new Employee();
                            Branch b = (Branch) branchComboBox.getSelectedItem();
                            Position p = (Position) positionComboBox.getSelectedItem();

                            if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty() || p == null || b == null) {
                                showMessageDialog(null, "Pola nie mogą być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            e.setName(nameTextField.getText());
                            e.setSurnameString(surnameTextField.getText());

                            Calendar calendar = Calendar.getInstance();
                            Date date = new Date(calendar.getTime().getTime());

                            e.setHireDate(date);
                            e.setBranch(b);
                            e.setPosition(p);

                            entityManager.getTransaction().begin();
                            entityManager.persist(e);
                            entityManager.getTransaction().commit();

                            etm.refresh();
                        }
                    }
                });
            }

            private void editEmployee(int idx) {

                Employee e = etm.employeeList.get(idx);

                Position p = etm.employeeList.get(idx).getPosition();

                Branch b = etm.employeeList.get(idx).getBranch();

                List<Branch> branches;
                List<Position> positions;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from Branch");
                branches = new ArrayList<>(q.getResultList());

                q = entityManager.createQuery("from Position");
                positions = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Imię: ");
                JTextField nameTextField = new JTextField("");

                JLabel surnnameLabel = new JLabel("Nazwisko: ");
                JTextField surnameTextField = new JTextField("");

                JLabel positionLabel = new JLabel("Stanowisko: ");
                JComboBox positionComboBox = new JComboBox<>(positions.toArray());

                JLabel branchLabel = new JLabel("Oddział: ");
                JComboBox branchComboBox = new JComboBox<>(branches.toArray());

                panel.setLayout(new GridLayout(6, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);
                nameTextField.setText(e.getName());

                panel.add(surnnameLabel);
                panel.add(surnameTextField);
                surnameTextField.setText(e.getSurnameString());

                panel.add(positionLabel);
                panel.add(positionComboBox);
                positionComboBox.setSelectedItem(p);

                panel.add(branchLabel);
                panel.add(branchComboBox);
                branchComboBox.setSelectedItem(b);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Branch b = (Branch) branchComboBox.getSelectedItem();
                            Position p = (Position) positionComboBox.getSelectedItem();

                            if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty() || p == null || b == null) {
                                showMessageDialog(null, "Pola nie mogą być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            e.setName(nameTextField.getText());
                            e.setSurnameString(surnameTextField.getText());

                            e.setBranch(b);
                            e.setPosition(p);

                            entityManager.getTransaction().begin();
                            entityManager.merge(e);
                            entityManager.getTransaction().commit();

                            etm.refresh();

                        }
                    }
                });
            }

            private void deleteEmployee(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Employee e = etm.employeeList.get(id);
                        entityManager.remove(e);
                    }
                    entityManager.getTransaction().commit();
                    etm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        etm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, "id musi być liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Imię") == 0) {
                    query = "FROM Employee e WHERE UPPER(e.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Nazwisko") == 0) {
                    query = "FROM Employee e WHERE UPPER(e.surname) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Oddział") == 0) {
                    query = "FROM Employee e WHERE UPPER(e.branch.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Stanowisko") == 0) {
                    query = "FROM Employee e WHERE UPPER(e.position.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Data zatrudnienia") == 0) {
                    if (param.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                        query = "FROM Employee e WHERE e.hireDate = :parameter";
                        etm.findByDateQuery(query, Date.valueOf(param));
                    } else {
                        showMessageDialog(null, "Podaj datę w formacie rrrr/MM/dd", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;

                }

                etm.findByQuery(query, param);
            }
        }
    }

    public class Rentals extends JFrame {

        JPanel panel, crudPanel, searchPanel;

        JButton addRentalButton, editRentalButton, deleteRentalButton,
                searchButton;

        JButton bookReturnedButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        RentalTableModel rtm;

        public Rentals() {
            rtm = new RentalTableModel();
            start();
        }

        public Rentals(int id) {
            rtm = new RentalTableModel();
            start();
            rtm.findByLibraryCardID(id);
        }

        public void start() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    rentalFrame = new JFrame("Wypożyczenia");

                    //rtm = new RentalTableModel();
                    table = new JTable(rtm);
                    table.setAutoCreateRowSorter(true);

                    rentalFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (rentalFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (rentalFrame.getHeight() / 2);

                    rentalFrame.setLocation(xPos, yPos);

                    rentalFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addRentalButton = new JButton("Dodaj wypożyczenie");
                    editRentalButton = new JButton("Edytuj wypożyczenie");
                    deleteRentalButton = new JButton("Usuń wypożyczenie");
                    searchButton = new JButton("Wyszukaj");
                    bookReturnedButton = new JButton("Zwrócono książkę");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = rtm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addRentalButton.addActionListener(lfb);
                    editRentalButton.addActionListener(lfb);
                    deleteRentalButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    bookReturnedButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Wypożyczenia"));

                    panel.add(new JScrollPane(table));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addRentalButton);
                    crudPanel.add(editRentalButton);
                    crudPanel.add(deleteRentalButton);
                    crudPanel.add(bookReturnedButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    rentalFrame.add(panel, BorderLayout.CENTER);
                    rentalFrame.add(crudPanel, BorderLayout.PAGE_START);
                    rentalFrame.add(searchPanel, BorderLayout.PAGE_END);

                    rentalFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    rentalFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addRentalButton) {
                    addRental();

                } else if (e.getSource() == editRentalButton) {
                    if (table.getSelectedRowCount() == 1) {
                        editRental(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteRentalButton) {
                    if (table.getSelectedRowCount() > 0) {
                        deleteRental(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        rtm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == bookReturnedButton) {
                    if (table.getSelectedRowCount() > 0) {
                        bookReturned(table.getSelectedRows());
                    }
                } else if (e.getSource() == refreshButton) {
                    rtm.refresh();
                }
            }

            private void bookReturned(int[] idx) {
                entityManager.getTransaction().begin();
                for (int id : idx) {
                    Rental r = rtm.rentalsList.get(id);

                    if (r.getReturnDate() != null) {
                        return;
                    }

                    Calendar calendar = Calendar.getInstance();
                    Date dateNow = new Date(calendar.getTime().getTime());

                    int rentedCount = r.getBook().getRentedCount();
                    r.getBook().setRentedCount(--rentedCount);
                    r.setReturnDate(dateNow);
                    entityManager.merge(r);
                }
                entityManager.getTransaction().commit();
                rtm.refresh();
            }

            private void addRental() {

                List<LibraryCard> libraryCards;
                List<Book> books;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from LibraryCard");
                libraryCards = new ArrayList<>(q.getResultList());

                q = entityManager.createQuery("SELECT b from Book b WHERE "
                        + "b.count - b.rentedCount > 0");
                books = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel bookLabel = new JLabel("Książka: ");
                JComboBox bookComboBox = new JComboBox<>(books.toArray());

                JLabel libraryCardLabel = new JLabel("ID karty: ");
                JComboBox libraryCardComboBox = new JComboBox<>(libraryCards.toArray());

                Calendar calendar = Calendar.getInstance();
                Date date = new Date(calendar.getTime().getTime());

                JLabel maxReturnDateLabel = new JLabel("Termin oddania: ");
                JSpinner maxReturnDateSpinner = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.DAY_OF_MONTH));
                JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(maxReturnDateSpinner, "dd.MM.yyyy");
                maxReturnDateSpinner.setEditor(dateEditor);

                panel.setLayout(new GridLayout(3, 2));

                panel.add(bookLabel);
                panel.add(bookComboBox);

                panel.add(libraryCardLabel);
                panel.add(libraryCardComboBox);

                panel.add(maxReturnDateLabel);
                panel.add(maxReturnDateSpinner);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj wypożyczenie",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Book b = (Book) bookComboBox.getSelectedItem();
                            LibraryCard lc = (LibraryCard) libraryCardComboBox.getSelectedItem();

                            if (b == null || lc == null) {
                                showMessageDialog(null, "Pole książka/karta biblioteczna nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            int rentedCount = b.getRentedCount();
                            b.setRentedCount(++rentedCount);

                            Rental r = new Rental();

                            Calendar calendar = Calendar.getInstance();
                            Date date = new Date(calendar.getTime().getTime());
                            java.util.Date spinnerDate = (java.util.Date) maxReturnDateSpinner.getModel().getValue();
                            Date maxReturnDate = new Date(spinnerDate.getTime());

                            r.setRentalDate(date);
                            r.setMaxReturnDate(maxReturnDate);
                            r.setBook(b);
                            r.setLibraryCard(lc);

                            entityManager.getTransaction().begin();
                            entityManager.persist(r);
                            entityManager.getTransaction().commit();

                            rtm.refresh();
                        }
                    }
                });
            }

            private void editRental(int idx) {

                Rental r = rtm.rentalsList.get(idx);

                LibraryCard lc = r.getLibraryCard();

                Book b = r.getBook();

                List<LibraryCard> libraryCards;
                List<Book> books;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from LibraryCard");
                libraryCards = new ArrayList<>(q.getResultList());

                q = entityManager.createQuery("SELECT b from Book b WHERE "
                        + "b.count - b.rentedCount > 0 "
                        + "OR b.bookID = " + b.getBookID());
                books = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel bookLabel = new JLabel("Książka: ");
                JComboBox bookComboBox = new JComboBox<>(books.toArray());

                JLabel libraryCardLabel = new JLabel("ID karty: ");
                JComboBox libraryCardComboBox = new JComboBox<>(libraryCards.toArray());

                JLabel maxReturnDateLabel = new JLabel("Termin oddania: ");
                JSpinner maxReturnDateSpinner = new JSpinner(new SpinnerDateModel(r.getMaxReturnDate(), null, null, Calendar.DAY_OF_MONTH));
                JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(maxReturnDateSpinner, "dd.MM.yyyy");
                maxReturnDateSpinner.setEditor(dateEditor);

                panel.setLayout(new GridLayout(3, 2));

                panel.add(bookLabel);
                panel.add(bookComboBox);
                bookComboBox.setSelectedItem(b);

                panel.add(libraryCardLabel);
                panel.add(libraryCardComboBox);
                libraryCardComboBox.setSelectedItem(lc);

                panel.add(maxReturnDateLabel);
                panel.add(maxReturnDateSpinner);
                maxReturnDateSpinner.getModel().setValue(r.getMaxReturnDate());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Book b = (Book) bookComboBox.getSelectedItem();
                            LibraryCard lc = (LibraryCard) libraryCardComboBox.getSelectedItem();

                            if (b == null || lc == null) {
                                showMessageDialog(null, "Pole książka/karta biblioteczna nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            r.setBook(b);
                            r.setLibraryCard(lc);
                            r.setMaxReturnDate((Date) maxReturnDateSpinner.getModel().getValue());

                            entityManager.getTransaction().begin();
                            entityManager.merge(r);
                            entityManager.getTransaction().commit();

                            rtm.refresh();
                        }
                    }
                });
            }

            private void deleteRental(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Rental r = rtm.rentalsList.get(id);
                        int rentedCount = r.getBook().getRentedCount();
                        r.getBook().setRentedCount(--rentedCount);
                        entityManager.remove(r);
                    }
                    entityManager.getTransaction().commit();
                    rtm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        rtm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Nr karty klienta") == 0) {
                    rtm.findByLibraryCardID(Integer.parseInt(param));
                    return;
                } else if (column.compareTo("Książka") == 0) {
                    query = "FROM Rental r WHERE UPPER(r.book.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Data złożenia") == 0) {
                    if (param.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                        query = "FROM Rental r WHERE r.rentalDate = :parameter";
                        rtm.findByDateQuery(query, Date.valueOf(param));
                        return;
                    } else {
                        showMessageDialog(null, "Data musi być w formacie yyyy-MM-dd", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }

                } else if (column.compareTo("Termina oddania") == 0) {

                    if (param.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                        query = "FROM Rental r WHERE r.maxReturnDate = :parameter";
                        rtm.findByDateQuery(query, Date.valueOf(param));
                        return;
                    } else {
                        showMessageDialog(null, "Data musi być w formacie yyyy-MM-dd", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }

                } else if (column.compareTo("Data oddania") == 0) {

                    if (param.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                        query = "FROM Rental r WHERE r.returnDate = :parameter";
                        rtm.findByDateQuery(query, Date.valueOf(param));
                        return;
                    } else {
                        showMessageDialog(null, "Data musi być w formacie yyyy-MM-dd", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                }
                if (!query.isEmpty()) {
                    rtm.findByQuery(query, param);
                }
            }

        }
    }

    public class LibraryCards extends JFrame {

        JPanel panel, crudPanel, searchPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        LibraryCardTableModel lctm;

        public LibraryCards() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    libraryCardFrame = new JFrame("Karty biblioteczne");

                    lctm = new LibraryCardTableModel();

                    table = new JTable(lctm);
                    table.setAutoCreateRowSorter(true);

                    libraryCardFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (libraryCardFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (libraryCardFrame.getHeight() / 2);

                    libraryCardFrame.setLocation(xPos, yPos);

                    libraryCardFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj karte biblioteczna");
                    editButton = new JButton("Edytuj karte biblioteczna");
                    deleteButton = new JButton("Usuń karte biblioteczna");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);
                    searchField.setBackground(Color.WHITE);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = lctm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    ListenForButton lfb = new ListenForButton();

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Karty biblioteczne"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    libraryCardFrame.add(panel, BorderLayout.CENTER);
                    libraryCardFrame.add(crudPanel, BorderLayout.PAGE_START);
                    libraryCardFrame.add(searchPanel, BorderLayout.PAGE_END);

                    libraryCardFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    libraryCardFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        lctm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    lctm.refresh();
                }
            }

            private void add() {
                List<Client> clients;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("SELECT c "
                        + "FROM Client c "
                        + "WHERE c NOT IN(SELECT lc.client "
                        + "FROM LibraryCard lc)");
                clients = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel clientLabel = new JLabel("ID klienta: ");
                JComboBox clientComboBox = new JComboBox<>(clients.toArray());

                JLabel themeLabel = new JLabel("Motyw: ");
                JTextField themeTextField = new JTextField();

                panel.setLayout(new GridLayout(3, 2));

                panel.add(clientLabel);
                panel.add(clientComboBox);

                panel.add(themeLabel);
                panel.add(themeTextField);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowa karte biblioteczną",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Client c = (Client) clientComboBox.getSelectedItem();

                            if (c == null) {
                                showMessageDialog(null, "Pole klient nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            LibraryCard lc = new LibraryCard();
                            lc.setClient(c);
                            lc.setTheme(themeTextField.getText());

                            Calendar calendar = Calendar.getInstance();
                            Date d = new Date(calendar.getTime().getTime());

                            lc.setCreationDate(d);

                            entityManager.getTransaction().begin();
                            entityManager.persist(lc);
                            entityManager.getTransaction().commit();

                            lctm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                LibraryCard lc = lctm.libraryCardsList.get(idx);

                Client c = lc.getClient();

                List<Client> clients;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("SELECT c "
                        + "FROM Client c "
                        + "WHERE c NOT IN(SELECT lc.client "
                        + "FROM LibraryCard lc) "
                        + "OR c.clientID = " + c.getClientID());
                clients = new ArrayList<>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel clientLabel = new JLabel("ID klienta: ");
                JComboBox clientComboBox = new JComboBox<>(clients.toArray());

                JLabel themeLabel = new JLabel("Motyw: ");
                JTextField themeTextField = new JTextField();

                panel.setLayout(new GridLayout(3, 2));

                panel.add(clientLabel);
                panel.add(clientComboBox);
                clientComboBox.setSelectedItem(c);

                panel.add(themeLabel);
                panel.add(themeTextField);
                themeTextField.setText(lc.getTheme());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowa karte biblioteczną",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Client c = (Client) clientComboBox.getSelectedItem();

                            if (c == null) {
                                showMessageDialog(null, "Pole klient nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            lc.setClient(c);
                            lc.setTheme(themeTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.merge(lc);
                            entityManager.getTransaction().commit();

                            lctm.refresh();
                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        LibraryCard lc = lctm.libraryCardsList.get(id);
                        entityManager.remove(lc);
                    }
                    entityManager.getTransaction().commit();
                    lctm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        lctm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Liczba wypożyczeń") == 0) {
                    lctm.findByRentalsCount(Long.parseLong(param));
                    return;
                } else if (column.compareTo("id Klienta") == 0) {
                    query = "FROM LibraryCard lc WHERE lc.clientID=:parameter)";
                } else if (column.compareTo("Data założenia") == 0) {
                    if (param.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                        query = "FROM LibraryCard lc WHERE lc.creationDate = :parameter";
                        lctm.findByDateQuery(query, Date.valueOf(param));
                    } else {
                        showMessageDialog(null, "Data musi być w formacie yyyy-MM-dd", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }

                    return;
                } else if (column.compareTo("Motyw") == 0) {
                    query = "FROM LibraryCard lc WHERE UPPER(lc.theme) LIKE UPPER(:parameter)";
                }

                lctm.findByQuery(query, param);
            }
        }
    }

    public class Authors extends JFrame {

        JPanel panel, crudPanel, searchPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        AuthorTableModel atm;

        public Authors() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    authorsFrame = new JFrame("Autorzy");

                    atm = new AuthorTableModel();

                    table = new JTable(atm);
                    table.setAutoCreateRowSorter(true);

                    authorsFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (authorsFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (authorsFrame.getHeight() / 2);

                    authorsFrame.setLocation(xPos, yPos);

                    authorsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj autora");
                    editButton = new JButton("Edytuj autora");
                    deleteButton = new JButton("Usuń autora");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = atm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0 || filter.compareTo("Ilość książek") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Autorzy"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    authorsFrame.add(panel, BorderLayout.CENTER);
                    authorsFrame.add(crudPanel, BorderLayout.PAGE_START);
                    authorsFrame.add(searchPanel, BorderLayout.PAGE_END);

                    authorsFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    authorsFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        atm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    atm.refresh();
                }
            }

            private void add() {

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Imię: ");
                JTextField nameTextField = new JTextField();

                JLabel surnameLabel = new JLabel("Nazwisko: ");
                JTextField surnameTextField = new JTextField();

                panel.setLayout(new GridLayout(2, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);

                panel.add(surnameLabel);
                panel.add(surnameTextField);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj autora",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Author a = new Author();
                            List<Book> b = new ArrayList<Book>();

                            if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole imie/nazwisko nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            a.setName(nameTextField.getText());
                            a.setSurname(surnameTextField.getText());
                            a.setBooks(b);

                            entityManager.getTransaction().begin();
                            entityManager.persist(a);
                            entityManager.getTransaction().commit();

                            atm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                Author a = atm.authorsList.get(idx);

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Imię: ");
                JTextField nameTextField = new JTextField();

                JLabel surnameLabel = new JLabel("Nazwisko: ");
                JTextField surnameTextField = new JTextField();

                panel.setLayout(new GridLayout(2, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);
                nameTextField.setText(a.getName());

                panel.add(surnameLabel);
                panel.add(surnameTextField);
                surnameTextField.setText(a.getSurname());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj autora",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            if (nameTextField.getText().isEmpty() || surnameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole imie/nazwisko nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            a.setName(nameTextField.getText());
                            a.setSurname(surnameTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.merge(a);
                            entityManager.getTransaction().commit();

                            atm.refresh();
                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Author a = atm.authorsList.get(id);
                        entityManager.remove(a);
                    }
                    entityManager.getTransaction().commit();
                    atm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        atm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Imię") == 0) {
                    query = "FROM Author a WHERE UPPER(a.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Nazwisko") == 0) {
                    query = "FROM Author a WHERE UPPER(a.surname) LIKE UPPER(:parameter)";
                //} else if (column.compareTo("Ilość książek") == 0)
                // query = "FROM Employee e WHERE UPPER(e.branch.name) LIKE UPPER(:parameter)";
                }

                atm.findByQuery(query, param);
            }
        }
    }

    public class Books extends JFrame {

        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        BookTableModel bktm;

        public Books() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    booksFrame = new JFrame("Książki");

                    bktm = new BookTableModel();

                    table = new JTable(bktm);
                    table.setAutoCreateRowSorter(true);

                    booksFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (booksFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (booksFrame.getHeight() / 2);

                    booksFrame.setLocation(xPos, yPos);

                    booksFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj książkę");
                    editButton = new JButton("Edytuj książkę");
                    deleteButton = new JButton("Usuń książkę");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = bktm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Książki"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    booksFrame.add(panel, BorderLayout.CENTER);
                    booksFrame.add(crudPanel, BorderLayout.PAGE_START);
                    booksFrame.add(searchPanel, BorderLayout.PAGE_END);

                    booksFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    booksFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        bktm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    bktm.refresh();
                }
            }

            private void add() {
                List<Branch> branches;
                List<Author> authors;
                List<Genre> genres;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from Branch");
                branches = new ArrayList<Branch>(q.getResultList());

                q = entityManager.createQuery("from Author");
                authors = new ArrayList<Author>(q.getResultList());

                q = entityManager.createQuery("from Genre");
                genres = new ArrayList<Genre>(q.getResultList());

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel titleLabel = new JLabel("Tytuł: ");
                JTextField titleTextField = new JTextField();

                JLabel yearLabel = new JLabel("Rok wydania: ");
                JTextField yearTextField = new JTextField();

                JLabel countLabel = new JLabel("Ilość: ");
                JTextField countTextField = new JTextField();

                JLabel branchLabel = new JLabel("Oddział: ");
                JComboBox branchComboBox = new JComboBox<>(branches.toArray());

                JLabel genreLabel = new JLabel("Gatunek: ");
                JComboBox genreComboBox = new JComboBox<>(genres.toArray());

                JLabel authorsLabel = new JLabel("Autorzy: ");
                JList authorsList = new JList(authors.toArray());

                panel.setLayout(new BorderLayout(2, 2));

                JPanel inputsPanel = new JPanel();
                inputsPanel.setLayout(new GridLayout(5, 2));

                inputsPanel.add(titleLabel);
                inputsPanel.add(titleTextField);

                inputsPanel.add(yearLabel);
                inputsPanel.add(yearTextField);

                inputsPanel.add(countLabel);
                inputsPanel.add(countTextField);

                inputsPanel.add(branchLabel);
                inputsPanel.add(branchComboBox);

                inputsPanel.add(genreLabel);
                inputsPanel.add(genreComboBox);

                panel.add(inputsPanel, BorderLayout.PAGE_START);

                JPanel authorsPanel = new JPanel();

                authorsPanel.add(authorsLabel);
                authorsPanel.add(new JScrollPane(authorsList));

                panel.add(authorsPanel);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowa książkę",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Branch b = (Branch) branchComboBox.getSelectedItem();
                            Genre g = (Genre) genreComboBox.getSelectedItem();
                            List<Author> a = authorsList.getSelectedValuesList();

                            if (b == null || g == null || a.isEmpty() || titleTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pola nie mogą być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }
                            if (!isNumeric(yearTextField.getText()) || !isNumeric(countTextField.getText())) {
                                showMessageDialog(null, "Wartośći rok wydania/ilość muszą być liczbami całkowitymi", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            Book bk = new Book();
                            bk.setAuthors(a);
                            bk.setBranch(b);
                            bk.setGenre(g);
                            bk.setName(titleTextField.getText());
                            bk.setReleaseYear(Integer.parseInt(yearTextField.getText()));
                            bk.setCount(Integer.parseInt(countTextField.getText()));

                            entityManager.getTransaction().begin();
                            entityManager.persist(bk);
                            entityManager.getTransaction().commit();

                            bktm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                Book bk = bktm.booksList.get(idx);

                List<Author> currentAuthors = bk.getAuthors();

                Branch b = bk.getBranch();

                Genre g = bk.getGenre();

                List<Branch> branches;
                List<Author> authors;
                List<Genre> genres;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery("from Branch");
                branches = new ArrayList<Branch>(q.getResultList());

                q = entityManager.createQuery("from Author");
                authors = new ArrayList<Author>(q.getResultList());

                q = entityManager.createQuery("from Genre");
                genres = new ArrayList<Genre>(q.getResultList());

                entityManager.getTransaction().commit();

                int indices[] = new int[currentAuthors.size()];

                for (int i = 0; i < currentAuthors.size(); i++) {
                    int ind = authors.indexOf(currentAuthors.get(i));
                    indices[i] = ind;
                }

                JPanel panel = new JPanel();

                JLabel titleLabel = new JLabel("Tytuł: ");
                JTextField titleTextField = new JTextField();

                JLabel yearLabel = new JLabel("Rok wydania: ");
                JTextField yearTextField = new JTextField();

                JLabel countLabel = new JLabel("Ilość: ");
                JTextField countTextField = new JTextField();

                JLabel branchLabel = new JLabel("Oddział: ");
                JComboBox branchComboBox = new JComboBox<>(branches.toArray());

                JLabel genreLabel = new JLabel("Gatunek: ");
                JComboBox genreComboBox = new JComboBox<>(genres.toArray());

                JLabel authorsLabel = new JLabel("Autorzy: ");
                JList authorsList = new JList(authors.toArray());

                panel.setLayout(new BorderLayout(2, 2));

                JPanel inputsPanel = new JPanel();
                inputsPanel.setLayout(new GridLayout(5, 2));

                inputsPanel.add(titleLabel);
                inputsPanel.add(titleTextField);
                titleTextField.setText(bk.getName());

                inputsPanel.add(yearLabel);
                inputsPanel.add(yearTextField);
                yearTextField.setText(Integer.toString(bk.getReleaseYear()));

                inputsPanel.add(countLabel);
                inputsPanel.add(countTextField);
                countTextField.setText(Integer.toString(bk.getCount()));

                inputsPanel.add(branchLabel);
                inputsPanel.add(branchComboBox);
                branchComboBox.setSelectedItem(b);

                inputsPanel.add(genreLabel);
                inputsPanel.add(genreComboBox);
                genreComboBox.setSelectedItem(g);

                panel.add(inputsPanel, BorderLayout.PAGE_START);

                JPanel authorsPanel = new JPanel();

                authorsPanel.add(authorsLabel);
                authorsList.setSelectedIndices(indices);
                authorsPanel.add(new JScrollPane(authorsList));

                panel.add(authorsPanel);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj książkę",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            Branch b = (Branch) branchComboBox.getSelectedItem();
                            Genre g = (Genre) genreComboBox.getSelectedItem();
                            List<Author> a = authorsList.getSelectedValuesList();

                            if (b == null || g == null || a.isEmpty() || titleTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pola nie mogą być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }
                            if (!isNumeric(yearTextField.getText()) || !isNumeric(countTextField.getText())) {
                                showMessageDialog(null, "Wartośći rok wydania/ilość muszą być liczbami całkowitymi", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            bk.setAuthors(a);
                            bk.setBranch(b);
                            bk.setGenre(g);
                            bk.setName(titleTextField.getText());
                            bk.setReleaseYear(Integer.parseInt(yearTextField.getText()));
                            bk.setCount(Integer.parseInt(countTextField.getText()));

                            entityManager.getTransaction().begin();
                            entityManager.merge(bk);
                            entityManager.getTransaction().commit();

                            bktm.refresh();

                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Book bk = bktm.booksList.get(id);
                        entityManager.remove(bk);
                    }
                    entityManager.getTransaction().commit();
                    bktm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        bktm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Rok wydania") == 0) {
                    if (isNumeric(param)) {
                        bktm.findByReleaseYear(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Ilość") == 0) {
                    if (isNumeric(param)) {
                        bktm.findByCount(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Ilość wypożyczonych") == 0) {
                    if (isNumeric(param)) {
                        bktm.findByRentedCount(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Autorzy") == 0) {
                    bktm.findByAuthor(param);
                    return;
                } else if (column.compareTo("Oddział") == 0) {
                    query = "FROM Book bk WHERE UPPER(bk.branch.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Tytuł") == 0) {
                    query = "FROM Book bk WHERE UPPER(bk.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Gatunek") == 0) {
                    query = "FROM Book bk WHERE UPPER(bk.genre.name) LIKE UPPER(:parameter)";
                }

                bktm.findByQuery(query, param);
            }
        }
    }

    public class Genres extends JFrame {

        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        GenreTableModel gtm;

        public Genres() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    genresFrame = new JFrame("Gatunki");

                    gtm = new GenreTableModel();

                    table = new JTable(gtm);
                    table.setAutoCreateRowSorter(true);

                    genresFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (genresFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (genresFrame.getHeight() / 2);

                    genresFrame.setLocation(xPos, yPos);

                    genresFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj gatunek");
                    editButton = new JButton("Edytuj gatunek");
                    deleteButton = new JButton("Usuń gatunek");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = gtm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Gatunki"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    genresFrame.add(panel, BorderLayout.CENTER);
                    genresFrame.add(crudPanel, BorderLayout.PAGE_START);
                    genresFrame.add(searchPanel, BorderLayout.PAGE_END);

                    genresFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    genresFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        gtm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    gtm.refresh();
                }
            }

            private void add() {

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Nazwa: ");
                JTextField nameTextField = new JTextField();

                panel.setLayout(new GridLayout(1, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowy gatunek",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            if (nameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole nazwy nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            Genre g = new Genre();
                            g.setName(nameTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.persist(g);
                            entityManager.getTransaction().commit();

                            gtm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                Genre g = gtm.genresList.get(idx);

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Nazwa: ");
                JTextField nameTextField = new JTextField();

                panel.setLayout(new GridLayout(1, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);
                nameTextField.setText(g.getName());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj gatunek",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            if (nameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole nazwy nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            g.setName(nameTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.merge(g);
                            entityManager.getTransaction().commit();

                            gtm.refresh();
                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Genre g = gtm.genresList.get(id);
                        entityManager.remove(g);
                    }
                    entityManager.getTransaction().commit();
                    gtm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        gtm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Nazwa") == 0) {
                    gtm.findByName(param);
                    return;
                } else if (column.compareTo("Ilość książek") == 0) {
                    if (isNumeric(param)) {
                        gtm.findByBooksCount(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                }
            }
        }
    }

    public class ReadingRooms extends JFrame {

        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        ReadingRoomsTableModel rrtm;

        public ReadingRooms() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    readingRoomsFrame = new JFrame("Czytelnie");

                    rrtm = new ReadingRoomsTableModel();

                    table = new JTable(rrtm);
                    table.setAutoCreateRowSorter(true);

                    readingRoomsFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (readingRoomsFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (readingRoomsFrame.getHeight() / 2);

                    readingRoomsFrame.setLocation(xPos, yPos);

                    readingRoomsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj czytelnie");
                    editButton = new JButton("Edytuj czytelnie");
                    deleteButton = new JButton("Usuń czytelnie");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = rrtm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Czytelnie"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    readingRoomsFrame.add(panel, BorderLayout.CENTER);
                    readingRoomsFrame.add(crudPanel, BorderLayout.PAGE_START);
                    readingRoomsFrame.add(searchPanel, BorderLayout.PAGE_END);

                    readingRoomsFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    readingRoomsFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        rrtm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    rrtm.refresh();
                }
            }
        }

        private void add() {

            List<Branch> branches;

            entityManager.getTransaction().begin();

            Query q = entityManager.createQuery("SELECT b "
                    + "FROM Branch b "
                    + "WHERE b NOT IN(SELECT rr.branch "
                    + "FROM ReadingRoom rr)");
            branches = new ArrayList<>(q.getResultList());

            entityManager.getTransaction().commit();

            if (branches.isEmpty()) {

                showMessageDialog(null, "Braku oddziału do którego można by dodać czytelnię", "Błąd", JOptionPane.WARNING_MESSAGE);
                return;
            }

            JPanel panel = new JPanel();

            JLabel branchLabel = new JLabel("Oddział: ");
            JComboBox branchComboBox = new JComboBox<>(branches.toArray());

            JLabel slotsLabel = new JLabel("Ilość miejsc: ");
            JTextField slotsTextField = new JTextField();

            panel.setLayout(new GridLayout(2, 2));

            panel.add(branchLabel);
            panel.add(branchComboBox);

            panel.add(slotsLabel);
            panel.add(slotsTextField);

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nową czytelnie",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        Branch b = (Branch) branchComboBox.getSelectedItem();

                        if (b == null) {
                            showMessageDialog(null, "Oddział nie może być pusty", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }
                        if (!isNumeric(slotsTextField.getText())) {
                            showMessageDialog(null, "Wartość ilości miejsc musi być liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        ReadingRoom rr = new ReadingRoom();
                        rr.setBranch(b);
                        rr.setSlots(Integer.parseInt(slotsTextField.getText()));
                        rr.setFreeSlots(rr.getSlots());

                        entityManager.getTransaction().begin();
                        entityManager.persist(rr);
                        entityManager.getTransaction().commit();

                        rrtm.refresh();

                    }
                }
            });
        }

        private void edit(int idx) {

            ReadingRoom rr = rrtm.readingRoomsList.get(idx);
            Branch b = rr.getBranch();

            List<Branch> branches;

            entityManager.getTransaction().begin();

            Query q = entityManager.createQuery("SELECT b "
                    + "FROM Branch b "
                    + "WHERE b NOT IN(SELECT rr.branch "
                    + "FROM ReadingRoom rr) "
                    + "OR b.branchID = " + b.getBranchID());
            branches = new ArrayList<>(q.getResultList());

            entityManager.getTransaction().commit();

            JPanel panel = new JPanel();

            JLabel branchLabel = new JLabel("Oddział: ");
            JComboBox branchComboBox = new JComboBox<>(branches.toArray());

            JLabel slotsLabel = new JLabel("Ilość miejsc: ");
            JTextField slotsTextField = new JTextField();

            panel.setLayout(new GridLayout(2, 2));

            panel.add(branchLabel);
            panel.add(branchComboBox);
            branchComboBox.setSelectedItem(b);

            panel.add(slotsLabel);
            panel.add(slotsTextField);
            slotsTextField.setText(Integer.toString(rr.getSlots()));

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj czytelnie",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (selection == JOptionPane.OK_OPTION) {

                        Branch b = (Branch) branchComboBox.getSelectedItem();

                        if (b == null) {
                            showMessageDialog(null, "Oddział nie może być pusty", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }
                        if (!isNumeric(slotsTextField.getText())) {
                            showMessageDialog(null, "Wartość ilości miejsc musi być liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                            return;
                        }

                        rr.setBranch(b);
                        rr.setSlots(Integer.parseInt(slotsTextField.getText()));
                        rr.setFreeSlots(rr.getSlots());

                        entityManager.getTransaction().begin();
                        entityManager.merge(rr);
                        entityManager.getTransaction().commit();

                        rrtm.refresh();
                    }
                }
            });
        }

        private void delete(int[] idx) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                entityManager.getTransaction().begin();
                for (int id : idx) {
                    ReadingRoom rr = rrtm.readingRoomsList.get(id);
                    entityManager.remove(rr);
                }
                entityManager.getTransaction().commit();
                rrtm.refresh();
            }
        }

        private void searchByColumn(String param, String column) {
            if (column.compareTo("id") == 0) {
                if (isNumeric(param)) {
                    rrtm.findByID(Integer.parseInt(param));
                } else {
                    showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                }
                return;
            } else if (column.compareTo("Oddział") == 0) {
                rrtm.findByBranchName(param);
                return;
            } else if (column.compareTo("Ilość miejsc") == 0) {
                if (isNumeric(param)) {
                    rrtm.findBySlots(Integer.parseInt(param));
                } else {
                    showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                }
                return;
            } else if (column.compareTo("Ilość wolnych miejsc") == 0) {
                if (isNumeric(param)) {
                    rrtm.findByFreeSlots(Integer.parseInt(param));
                } else {
                    showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                }
                return;
            }
        }
    }

    public class Registers extends JFrame {

        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JButton clientExitButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        RegisterTableModel rtm;

        public Registers() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    registersFrame = new JFrame("Rejestry");

                    rtm = new RegisterTableModel();

                    table = new JTable(rtm);
                    table.setAutoCreateRowSorter(true);

                    registersFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (registersFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (registersFrame.getHeight() / 2);

                    registersFrame.setLocation(xPos, yPos);

                    registersFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj rejestr");
                    editButton = new JButton("Edytuj rejestr");
                    deleteButton = new JButton("Usuń rejestr");
                    searchButton = new JButton("Wyszukaj");
                    clientExitButton = new JButton("Klient wyszedł");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);
                    searchField.setBackground(Color.WHITE);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = rtm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    clientExitButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Rejestry"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(clientExitButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    registersFrame.add(panel, BorderLayout.CENTER);
                    registersFrame.add(crudPanel, BorderLayout.PAGE_START);
                    registersFrame.add(searchPanel, BorderLayout.PAGE_END);

                    registersFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    registersFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        rtm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == clientExitButton) {
                    if (table.getSelectedRowCount() > 0) {
                        clientsLeft(table.getSelectedRows());
                    }
                } else if (e.getSource() == refreshButton) {
                    rtm.refresh();
                }
            }

            private void clientsLeft(int[] idx) {
                Calendar calendar = Calendar.getInstance();

                Timestamp ts = new Timestamp(calendar.getTime().getTime());
                entityManager.getTransaction().begin();

                for (int id : idx) {
                    Registry r = rtm.registersList.get(id);
                    ReadingRoom rr = r.getReadingRoom();
                    if (r.getLeftDate() == null) {
                        r.setLeftDate(ts);
                        rr.setFreeSlots((rr.getFreeSlots() + 1));
                        entityManager.merge(r);
                        entityManager.merge(rr);
                    }
                }
                entityManager.getTransaction().commit();
                rtm.refresh();
            }

            private void add() {

                List<ReadingRoom> readingRooms;
                List<LibraryCard> libraryCards;

                entityManager.getTransaction().begin();

                Query q = entityManager.createQuery(""
                        + "SELECT lc "
                        + "FROM LibraryCard lc "
                        + "WHERE lc NOT IN ("
                        + "SELECT r.libraryCard "
                        + "FROM Registry r "
                        + "WHERE r.leftDate IS NULL)");
                libraryCards = new ArrayList<>(q.getResultList());

                q = entityManager.createQuery("SELECT rr "
                        + "FROM ReadingRoom rr "
                        + "WHERE rr.freeSlots > 0");
                readingRooms = new ArrayList<>(q.getResultList());

                if (libraryCards.isEmpty()) {
                    showMessageDialog(null, "Brak osób do wejścia do czytelni", "Błąd", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                if (readingRooms.isEmpty()) {
                    showMessageDialog(null, "Braku wolnych miejsc w czytelniach", "Błąd", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                entityManager.getTransaction().commit();

                JPanel panel = new JPanel();

                JLabel readingRoomLabel = new JLabel("Czytelnia: ");
                JComboBox readingRoomComboBox = new JComboBox<>(readingRooms.toArray());

                JLabel libraryCardLabel = new JLabel("Nr karty: ");
                JComboBox libraryCardComboBox = new JComboBox<>(libraryCards.toArray());

                panel.setLayout(new GridLayout(3, 2));

                panel.add(readingRoomLabel);
                panel.add(readingRoomComboBox);

                panel.add(libraryCardLabel);
                panel.add(libraryCardComboBox);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowy rejestr",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            ReadingRoom rr = (ReadingRoom) readingRoomComboBox.getSelectedItem();
                            LibraryCard lc = (LibraryCard) libraryCardComboBox.getSelectedItem();

                            if (rr == null) {
                                showMessageDialog(null, "Należy wybrać czytelnię", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }
                            if (lc == null) {
                                showMessageDialog(null, "Należy wybrać kartę biblioteczną", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            rr.setFreeSlots((rr.getFreeSlots() - 1));

                            Registry r = new Registry();
                            r.setReadingRoom(rr);
                            r.setLibraryCard(lc);

                            Calendar calendar = Calendar.getInstance();

                            Timestamp ts = new Timestamp(calendar.getTime().getTime());
                            r.setEnterDate(ts);

                            entityManager.getTransaction().begin();
                            entityManager.persist(r);
                            entityManager.getTransaction().commit();

                            rtm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                Registry r = rtm.registersList.get(idx);

                ReadingRoom rr = r.getReadingRoom();

                LibraryCard lc = r.getLibraryCard();

                List<ReadingRoom> readingRooms;
                List<LibraryCard> libraryCards;

                readingRooms = ReadingRoom.findAll(entityManager);
                libraryCards = LibraryCard.findAll(entityManager);

                JPanel panel = new JPanel();

                JLabel readingRoomLabel = new JLabel("Czytelnia: ");
                JComboBox readingRoomComboBox = new JComboBox<>(readingRooms.toArray());

                JLabel libraryCardLabel = new JLabel("Nr karty: ");
                JComboBox libraryCardComboBox = new JComboBox<>(libraryCards.toArray());

                JLabel enterLabel = new JLabel("Data wejścia: ");
                JTextField enterTextField = new JTextField();

                panel.setLayout(new GridLayout(3, 2));

                panel.add(readingRoomLabel);
                panel.add(readingRoomComboBox);
                readingRoomComboBox.setSelectedItem(rr);

                panel.add(libraryCardLabel);
                panel.add(libraryCardComboBox);
                libraryCardComboBox.setSelectedItem(lc);

                panel.add(enterLabel);
                panel.add(enterTextField);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj rejestr",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            ReadingRoom rr = (ReadingRoom) readingRoomComboBox.getSelectedItem();
                            LibraryCard lc = (LibraryCard) libraryCardComboBox.getSelectedItem();

                            r.setReadingRoom(rr);
                            r.setLibraryCard(lc);
                            //r.setEnterDate(enterDate);

                            entityManager.getTransaction().begin();
                            entityManager.persist(r);
                            entityManager.getTransaction().commit();

                            rtm.refresh();
                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Registry r = rtm.registersList.get(id);
                        entityManager.remove(r);
                    }
                    entityManager.getTransaction().commit();
                    rtm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                String query = new String();
                if (column.compareTo("id") == 0) {
                    rtm.findByID(Integer.parseInt(param));
                    return;
                } else if (column.compareTo("Czytelnia oddziału") == 0) {
                    query = "FROM Registry r WHERE UPPER(r.branch.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Imię") == 0) {
                    query = "FROM Registry r WHERE UPPER(r.libraryCard.client.name) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Nazwisko") == 0) {
                    query = "FROM Registry r WHERE UPPER(r.libraryCard.client.surname) LIKE UPPER(:parameter)";
                } else if (column.compareTo("Nr karty") == 0) {
                    rtm.findByLibraryCardID(Integer.parseInt(param));
                    return;
                } else if (column.compareTo("Data wejśćia") == 0) {

                } else if (column.compareTo("Data wyjśćia") == 0) {

                }
                rtm.findByQuery(query, param);
            }
        }
    }
    
    public class Positions extends JFrame {

        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton, refreshButton;

        JTextField searchField;
        JComboBox searchByComboBox;

        JTable table;
        PositionTableModel pttm;

        public Positions() {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {

                    positionsFrame = new JFrame("Stanowiska");

                    pttm = new PositionTableModel();

                    table = new JTable(pttm);
                    table.setAutoCreateRowSorter(true);

                    positionsFrame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (positionsFrame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (positionsFrame.getHeight() / 2);

                    positionsFrame.setLocation(xPos, yPos);

                    positionsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj stanowisko");
                    editButton = new JButton("Edytuj stanowisko");
                    deleteButton = new JButton("Usuń stanowisko");
                    searchButton = new JButton("Wyszukaj");
                    refreshButton = new JButton("🗘");

                    searchField = new JTextField();
                    Dimension d = searchField.getPreferredSize();
                    d.width = 200;
                    searchField.setPreferredSize(d);

                    List<String> searchFiltersList = new ArrayList<>();

                    String[] tmp = pttm.getColumnNames();
                    for (String filter : tmp) {
                        if (filter.compareTo("lp") != 0 && filter.compareTo("Ilość pracowników") != 0) {
                            searchFiltersList.add(filter);
                        }
                    }

                    searchByComboBox = new JComboBox<>(searchFiltersList.toArray());
                    Dimension d2 = searchByComboBox.getPreferredSize();
                    d2.width = 120;
                    searchByComboBox.setPreferredSize(d2);

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);
                    searchButton.addActionListener(lfb);
                    refreshButton.addActionListener(lfb);

                    panel = new JPanel(new BorderLayout());
                    crudPanel = new JPanel();
                    searchPanel = new JPanel();

                    panel.add(new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
                    panel.setBorder(BorderFactory.createTitledBorder("Stanowiska"));

                    crudPanel.setBorder(BorderFactory.createTitledBorder("Działania"));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);
                    crudPanel.add(refreshButton);

                    searchPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwanie"));
                    searchPanel.add(searchByComboBox);
                    searchPanel.add(searchField);
                    searchPanel.add(searchButton);

                    positionsFrame.add(panel, BorderLayout.CENTER);
                    positionsFrame.add(crudPanel, BorderLayout.PAGE_START);
                    positionsFrame.add(searchPanel, BorderLayout.PAGE_END);

                    positionsFrame.setVisible(true);

                    ListenForWindow listenForWindow = new ListenForWindow();
                    positionsFrame.addWindowListener(listenForWindow);
                }
            });
        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if (table.getSelectedRowCount() == 1) {
                        edit(table.getSelectedRow());
                    }

                } else if (e.getSource() == deleteButton) {
                    if (table.getSelectedRowCount() > 0) {
                        delete(table.getSelectedRows());
                    }
                } else if (e.getSource() == searchButton) {
                    String param = searchField.getText();
                    String c = (String) searchByComboBox.getSelectedItem();
                    if (param.isEmpty()) {
                        pttm.findAll();
                    } else {
                        searchByColumn(param, c);
                    }
                } else if (e.getSource() == refreshButton) {
                    pttm.refresh();
                }
            }

            private void add() {

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Nazwa: ");
                JTextField nameTextField = new JTextField();

                panel.setLayout(new GridLayout(1, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Dodaj nowe stanowisko",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            if (nameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole nazwy nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            Position p = new Position();
                            p.setName(nameTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.persist(p);
                            entityManager.getTransaction().commit();

                            pttm.refresh();

                        }
                    }
                });
            }

            private void edit(int idx) {

                Position p = pttm.positionsList.get(idx);

                JPanel panel = new JPanel();

                JLabel nameLabel = new JLabel("Nazwa: ");
                JTextField nameTextField = new JTextField();

                panel.setLayout(new GridLayout(1, 2));

                panel.add(nameLabel);
                panel.add(nameTextField);
                nameTextField.setText(p.getName());

                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int selection = JOptionPane.showConfirmDialog(null, panel, "Edytuj stanowisko",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                        if (selection == JOptionPane.OK_OPTION) {

                            if (nameTextField.getText().isEmpty()) {
                                showMessageDialog(null, "Pole nazwy nie może być puste", "Błąd", JOptionPane.WARNING_MESSAGE);
                                return;
                            }

                            p.setName(nameTextField.getText());

                            entityManager.getTransaction().begin();
                            entityManager.merge(p);
                            entityManager.getTransaction().commit();

                            pttm.refresh();
                        }
                    }
                });
            }

            private void delete(int[] idx) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz usunąć zaznaczone rekordy?", "Warning", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    entityManager.getTransaction().begin();
                    for (int id : idx) {
                        Position p = pttm.positionsList.get(id);
                        if(p.getEmployees() == null || p.getEmployees().size() == 0)
                        {
                            entityManager.remove(p);
                        }else
                            showMessageDialog(null, "Nie można usunąć '" + p.getName() + "' są na nim pracownicy", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    entityManager.getTransaction().commit();
                    pttm.refresh();
                }
            }

            private void searchByColumn(String param, String column) {
                if (column.compareTo("id") == 0) {
                    if (isNumeric(param)) {
                        pttm.findByID(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                } else if (column.compareTo("Nazwa") == 0) {
                    pttm.findByName(param);
                    return;
                } else if (column.compareTo("Ilość pracowników") == 0) {
                    if (isNumeric(param)) {
                        pttm.findByEmployeesCount(Integer.parseInt(param));
                    } else {
                        showMessageDialog(null, " '" + param + "' nie jest liczbą całkowitą", "Błąd", JOptionPane.WARNING_MESSAGE);
                    }
                    return;
                }
            }
        }
    }

    public class PersonTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825518764825L;
        private ArrayList<Client> clientList;
        private String[] columnNames;

        public PersonTableModel() {
            super();

            columnNames = new String[]{"lp", "id", /*"Nr karty"*/ "Imię", "Nazwisko", "Email", "Miasto", "Ulica", "Nr budynku"};

            findAll();

        }

        public int getRowCount() {
            return clientList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Client p = clientList.get(rowIndex);
            //LibraryCard lc = LibraryCard.findByClientID(entityManager, p);
            Object[] values;

            values = new Object[]{rowIndex + 1, p.getClientID(), p.getName(), p.getSurname(),
                p.getEmail(), p.getAddress().getCity(), p.getAddress().getStreet(), p.getAddress().getStreetNumber()};

            return values[columnIndex];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void setColumnNames(String[] columnNames) {
            this.columnNames = columnNames;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            clientList = Client.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            clientList = Client.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            clientList = Client.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            clientList = Client.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }
    }

    public class BranchTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825247544825L;
        private ArrayList<Branch> branchList;
        private String[] columnNames;

        public BranchTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Nazwa", "Miasto", "Ulica", "Nr budynku"};

            findAll();

        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public int getRowCount() {
            return branchList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Branch p = branchList.get(rowIndex);
            Address a = p.getAddress();
            Object[] values = new Object[]{rowIndex + 1, p.getBranchID(), p.getName(), a.getCity(), a.getStreet(),
                a.getStreetNumber()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            branchList = Branch.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            branchList = Branch.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            branchList = Branch.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            branchList = Branch.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }
    }

    public class EmployeeTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825247544825L;
        private ArrayList<Employee> employeeList;
        private String[] columnNames;

        public EmployeeTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Imię", "Nazwisko", "Data zatrudnienia", "Oddział", "Stanowisko"};

            findAll();

        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public int getRowCount() {
            return employeeList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Employee e = employeeList.get(rowIndex);

            Branch b = e.getBranch();

            Position p = e.getPosition();

            Object[] values = new Object[]{rowIndex + 1, e.getEmployeeID(), e.getName(), e.getSurnameString(), e.getHireDate(), b.getName(), p.getName(),};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            employeeList = Employee.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            employeeList = Employee.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            employeeList = Employee.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            employeeList = Employee.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }

        public void findByDateQuery(String q, Date date) {
            employeeList = Employee.findByDateQuery(entityManager, q, date);

            fireTableDataChanged();
        }
    }

    public class RentalTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825247544825L;
        private ArrayList<Rental> rentalsList;
        private String[] columnNames;

        public RentalTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Książka", "Nr karty klienta", "Data złożenia", "Termina oddania", "Data oddania"};

            findAll();
        }

        public int getRowCount() {
            return rentalsList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Rental r = rentalsList.get(rowIndex);

            Object[] values = new Object[]{rowIndex + 1, r.getRentalID(), r.getBook().getName(),
                r.getLibraryCard().getLibraryCardID(),
                r.getRentalDate(), r.getMaxReturnDate(), r.getReturnDate()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void refresh() {
            findAll();

        }

        public void findByLibraryCardID(int id) {
            rentalsList = Rental.findByLibraryCardID(entityManager, id);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            rentalsList = Rental.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            rentalsList = Rental.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            rentalsList = Rental.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }

        public void findByDateQuery(String q, Date date) {
            rentalsList = Rental.findByDateQuery(entityManager, q, date);

            fireTableDataChanged();
        }
    }

    public class LibraryCardTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<LibraryCard> libraryCardsList;
        private String[] columnNames;

        public LibraryCardTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "id Klienta", "Data założenia", "Motyw", "Liczba wypożyczeń"};

            findAll();
        }

        public int getRowCount() {
            return libraryCardsList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            LibraryCard lc = libraryCardsList.get(rowIndex);

            int rentalsCount = Rental.findByLibraryCardID(entityManager, lc.getLibraryCardID()).size();

            Object[] values = new Object[]{rowIndex + 1, lc.getLibraryCardID(), lc.getClient().getClientID(), lc.getCreationDate(), lc.getTheme(), rentalsCount};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void refresh() {
            findAll();
        }

        public void findByID(int id) {

            libraryCardsList = LibraryCard.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            libraryCardsList = LibraryCard.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            libraryCardsList = LibraryCard.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }

        public void findByRentalsCount(long id) {
            libraryCardsList = LibraryCard.findByRentalsCount(entityManager, id);

            fireTableDataChanged();
        }

        public void findByDateQuery(String q, Date date) {
            libraryCardsList = LibraryCard.findByDateQuery(entityManager, q, date);

            fireTableDataChanged();
        }
    }

    public class BookTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<Book> booksList;
        private String[] columnNames;

        public BookTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Oddział", "Tytuł", "Gatunek", "Rok wydania", "Ilość", "Ilość wypożyczonych", "Autorzy"};

            findAll();
        }

        public int getRowCount() {
            return booksList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Book b = booksList.get(rowIndex);

            List<Author> authors = b.getAuthors();
            List<Author> as = new Vector<Author>(authors);

            Object[] values = new Object[]{rowIndex + 1, b.getBookID(), b.getBranch(), b.getName(), b.getGenre(), b.getReleaseYear(), b.getCount(), b.getRentedCount(), as};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            booksList = Book.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            booksList = Book.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            booksList = Book.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            booksList = Book.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }

        public void findByReleaseYear(int year) {
            booksList = Book.findByReleaseYear(entityManager, year);

            fireTableDataChanged();
        }

        public void findByCount(int count) {
            booksList = Book.findByCount(entityManager, count);

            fireTableDataChanged();
        }

        public void findByRentedCount(int rentedCount) {
            booksList = Book.findByRentedCount(entityManager, rentedCount);

            fireTableDataChanged();
        }

        public void findByAuthor(String authorName) {
            booksList = Book.findByAuthor(entityManager, authorName);

            fireTableDataChanged();
        }

    }

    public class AuthorTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<Author> authorsList;
        private String[] columnNames;

        public AuthorTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Imię", "Nazwisko", "Ilość książek"};

            findAll();
        }

        public int getRowCount() {
            return authorsList.size();
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Author a = authorsList.get(rowIndex);

            List<Book> books = a.getBooks();

            Object[] values = new Object[]{rowIndex + 1, a.getAuthorID(), a.getName(), a.getSurname(), books.size()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            authorsList = Author.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            authorsList = Author.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            authorsList = Author.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByQuery(String q, String param) {
            authorsList = Author.findByQuery(entityManager, q, param);

            fireTableDataChanged();
        }

    }

    public class GenreTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<Genre> genresList;
        private String[] columnNames;

        public GenreTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Nazwa", "Ilość książek"};

            findAll();
        }

        public int getRowCount() {
            return genresList.size();
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Genre g = genresList.get(rowIndex);

            List<Book> books = g.getBooks();

            Object[] values = new Object[]{rowIndex + 1, g.getGenreID(), g.getName(), books.size()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            genresList = Genre.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByBooksCount(int count) {

            genresList = Genre.findByBooksCount(entityManager, count);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            genresList = Genre.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            genresList = Genre.findAll(entityManager);

            fireTableDataChanged();
        }
    }
    
    public class PositionTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<Position> positionsList;
        private String[] columnNames;

        public PositionTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Nazwa", "Ilość pracowników"};

            findAll();
        }

        public int getRowCount() {
            return positionsList.size();
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Position pt = positionsList.get(rowIndex);

           int employeesCount = Position.findEmployees(entityManager, pt.getPositionID()).size();

            Object[] values = new Object[]{rowIndex + 1, pt.getPositionID(), pt.getName(), employeesCount};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public void refresh() {
            findAll();
        }

        public void findByName(String name) {

            positionsList = Position.findByName(entityManager, name);

            fireTableDataChanged();
        }

        public void findByEmployeesCount(int count) {

            positionsList = Position.findByEmployeesCount(entityManager, count);

            fireTableDataChanged();
        }

        public void findByID(int id) {

            positionsList = Position.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findAll() {

            positionsList = Position.findAll(entityManager);

            fireTableDataChanged();
        }
    }

    public class ReadingRoomsTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<ReadingRoom> readingRoomsList;
        private String[] columnNames;

        public ReadingRoomsTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Oddział", "Ilość miejsc", "Ilość wolnych miejsc"};

            findAll();
        }

        public int getRowCount() {
            return readingRoomsList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            ReadingRoom rr = readingRoomsList.get(rowIndex);

            Object[] values = new Object[]{rowIndex + 1, rr.getReadingRoomID(), rr.getBranch().getName(), rr.getSlots(), rr.getFreeSlots()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void refresh() {
            findAll();
        }

        public void findAll() {
            readingRoomsList = ReadingRoom.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByID(int id) {
            readingRoomsList = ReadingRoom.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findBySlots(int slots) {
            readingRoomsList = ReadingRoom.findBySlots(entityManager, slots);

            fireTableDataChanged();
        }

        public void findByFreeSlots(int freeSlots) {
            readingRoomsList = ReadingRoom.findByFreeSlots(entityManager, freeSlots);

            fireTableDataChanged();
        }

        public void findByBranchName(String branchName) {
            readingRoomsList = ReadingRoom.findByBranchName(entityManager, branchName);

            fireTableDataChanged();
        }

    }

    public class RegisterTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 6105842825237544825L;
        private ArrayList<Registry> registersList;
        private String[] columnNames;

        public RegisterTableModel() {
            super();

            columnNames = new String[]{"lp", "id", "Czytelnia oddziału", "Imię", "Nazwisko", "Nr karty", "Data wejścia", "Data wyjścia"};

            findAll();
        }

        public int getRowCount() {
            return registersList.size();
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Registry r = registersList.get(rowIndex);

            Object[] values = new Object[]{rowIndex + 1, r.getRegistryID(), r.getReadingRoom().getBranch().getName(),
                r.getLibraryCard().getClient().getName(), r.getLibraryCard().getClient().getSurname(),
                r.getLibraryCard().getLibraryCardID(), r.getEnterDate(), r.getLeftDate()};
            return values[columnIndex];
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public void refresh() {
            findAll();
        }

        public void findAll() {
            registersList = Registry.findAll(entityManager);

            fireTableDataChanged();
        }

        public void findByID(int id) {
            registersList = Registry.findByID(entityManager, id);

            fireTableDataChanged();
        }

        public void findByLibraryCardID(int libraryCardID) {
            registersList = Registry.findByLibraryCardID(entityManager, libraryCardID);

            fireTableDataChanged();
        }

        public void findByQuery(String query, String param) {
            registersList = Registry.findByQuery(entityManager, query, param);

            fireTableDataChanged();
        }

    }

    private void openLibraryInfo() {
        infoFrame = new LibraryInfo();
    }

    private void openBranches() {
        branchesFrame = new Branches();
    }

    private void openClients() {
        clientsFrame = new Clients();
    }

    private void openEmployees() {
        employeesFrame = new Employees();
    }

    private void openRentals() {
        rentalFrame = new Rentals();
    }

    private void openBooks() {
        booksFrame = new Books();
    }

    private void openLibraryCard() {
        libraryCardFrame = new LibraryCards();
    }

    private void openAuthors() {
        authorsFrame = new Authors();
    }

    private void openGenres() {
        genresFrame = new Genres();
    }

    private void openReadingRooms() {
        readingRoomsFrame = new ReadingRooms();
    }

    private void openRegisters() {
        registersFrame = new Registers();
    }
    
    private void openPositions() {
        positionsFrame = new Positions();
    }

    public void populateDb() {

        Library library = new Library();
        library.setOpenTime(new Time(28800000));
        library.setCloseTime(new Time(64800000));

        entityManager.getTransaction().begin();
        entityManager.persist(library);
        entityManager.getTransaction().commit();

    }

    public static void main(String[] args) {

        Biblioteka b = new Biblioteka();
        b.populateDb();      
    }
}
