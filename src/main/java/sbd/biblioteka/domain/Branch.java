/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */

@Entity
public class Branch {
    
    @Id
    @GeneratedValue
    private int branchID;
    
    @Column
    private String name;
    
    @OneToOne
    @JoinColumn(name = "addressID")
    private Address address;
    
    @OneToMany(mappedBy = "branch")
    private List<Employee> employees;
    
    @ManyToOne
    @JoinColumn(name = "libraryID")
    private Library library;
    
    @OneToMany(mappedBy = "branch")
    private List<Book> books;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }
    
    
    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getBranchID() {
        return branchID;
    }

    public void setBranchID(int branchID) {
        this.branchID = branchID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    public static ArrayList<Branch> findByName(EntityManager em, String name) {

        ArrayList<Branch> result;

        em.getTransaction().begin();

        result = (ArrayList<Branch>) em.createQuery(
                "SELECT b FROM Branch b WHERE b.name LIKE :custName")
                .setParameter("custName", name)
                .getResultList();

        em.getTransaction().commit();

        return result;
    }
    
    public static ArrayList<Branch> findAll(EntityManager em){
        ArrayList<Branch> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Branch>) em.createQuery(
            "SELECT b FROM Branch b")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Branch> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Branch> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Branch>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Branch> findByID(EntityManager em, int id){
        ArrayList<Branch> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Branch>) em.createQuery(
            "SELECT b FROM Branch b WHERE b.branchID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
}
       
