/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */
@Entity
public class LibraryCard {
    
    @Id
    @GeneratedValue
    private int libraryCardID;
    
    @OneToOne
    @JoinColumn(name = "clientID")
    private Client client;
   
    @OneToMany(mappedBy = "libraryCard")
    private List<Rental> rentals;
    
    @Column
    private Date creationDate;
    

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    public int getLibraryCardID() {
        return libraryCardID;
    }

    public void setLibraryCardID(int libraryCardID) {
        this.libraryCardID = libraryCardID;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    
    @Column
    private String theme;
    
    @Override
    public String toString() {
        return Integer.toString(libraryCardID);
    }
    
    public static LibraryCard findByClientID(EntityManager em, Client id){
        
        ArrayList<LibraryCard> result;
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(
            "SELECT lc FROM LibraryCard lc WHERE lc.client = :clientID")
            .setParameter("clientID", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result.get(0);
    }
    
     public static ArrayList<LibraryCard> findAll(EntityManager em){
        ArrayList<LibraryCard> result;
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(
            "SELECT lc FROM LibraryCard lc")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<LibraryCard> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<LibraryCard> result;
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<LibraryCard> findByID(EntityManager em, int id){
        ArrayList<LibraryCard> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(
            "SELECT lc FROM LibraryCard lc WHERE lc.libraryCardID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<LibraryCard> findByRentalsCount(EntityManager em, long id){
        
        ArrayList<LibraryCard> result;
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(
            "SELECT lc FROM LibraryCard lc")
            /*.setParameter("id", id)*/
            .getResultList();
        
        System.err.println(result);

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<LibraryCard> findByDateQuery(EntityManager em, String q, Date date){
        ArrayList<LibraryCard> result;
        
        em.getTransaction().begin();

        result = (ArrayList<LibraryCard>) em.createQuery(q)
                .setParameter("parameter", date)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
