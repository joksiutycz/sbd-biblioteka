/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */
@Entity
public class ReadingRoom {
    @Id
    @GeneratedValue
    private int readingRoomID;
    
    @OneToOne
    @JoinColumn(name = "branchID")
    private Branch branch;
    
    private int slots;
    
    private int freeSlots;

    public int getFreeSlots() {
        return freeSlots;
    }

    public void setFreeSlots(int freeSlots) {
        this.freeSlots = freeSlots;
    }

    public int getReadingRoomID() {
        return readingRoomID;
    }

    public void setReadingRoomID(int readingRoomID) {
        this.readingRoomID = readingRoomID;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }
    
    public static ArrayList<ReadingRoom> findByID(EntityManager em, int id){
        
        ArrayList<ReadingRoom> result;
        
        em.getTransaction().begin();

        result = (ArrayList<ReadingRoom>) em.createQuery(
            "SELECT rr FROM ReadingRoom rr WHERE rr.readingRoomID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<ReadingRoom> findAll(EntityManager em){
        
        ArrayList<ReadingRoom> result;
        
        em.getTransaction().begin();

        result = (ArrayList<ReadingRoom>) em.createQuery(
            "SELECT rr FROM ReadingRoom rr")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    public static ArrayList<ReadingRoom> findBySlots(EntityManager em, int slots){
        
        ArrayList<ReadingRoom> result;
        
        em.getTransaction().begin();

        result = (ArrayList<ReadingRoom>) em.createQuery(
            "SELECT rr FROM ReadingRoom rr WHERE rr.slots=:slots")
            .setParameter("slots", slots)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    public static ArrayList<ReadingRoom> findByFreeSlots(EntityManager em, int freeSlots){
        
        ArrayList<ReadingRoom> result;
        
        em.getTransaction().begin();

        result = (ArrayList<ReadingRoom>) em.createQuery(
            "SELECT rr FROM ReadingRoom rr WHERE rr.freeSlots=:freeSlots")
            .setParameter("freeSlots", freeSlots)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<ReadingRoom> findByBranchName(EntityManager em, String branchName){
        
        ArrayList<ReadingRoom> result;
        
        em.getTransaction().begin();

        result = (ArrayList<ReadingRoom>) em.createQuery(
            "SELECT rr FROM ReadingRoom rr WHERE UPPER(rr.branch.name) LIKE UPPER(:branchName)")
            .setParameter("branchName", branchName)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    @Override
    public String toString(){
        return Integer.toString(readingRoomID);
    }
}
