/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */
@Entity
public class Rental {
    @Id
    @GeneratedValue
    private int rentalID;
    
    @OneToOne
    @JoinColumn(name = "bookID")
    private Book book;
    
    @ManyToOne
    @JoinColumn(name = "libraryCardID")
    private LibraryCard libraryCard;
    
    private Date rentalDate;

    private Date returnDate;
    
    private Date maxReturnDate;
    
    

    public LibraryCard getLibraryCard() {
        return libraryCard;
    }

    public void setLibraryCard(LibraryCard libraryCard) {
        this.libraryCard = libraryCard;
    }
    

    public int getRentalID() {
        return rentalID;
    }

    public void setRentalID(int rentalID) {
        this.rentalID = rentalID;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getRentalDate() {
        return rentalDate;
    }

    public void setRentalDate(Date rentalDate) {
        this.rentalDate = rentalDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getMaxReturnDate() {
        return maxReturnDate;
    }

    public void setMaxReturnDate(Date maxReturnDate) {
        this.maxReturnDate = maxReturnDate;
    }
    
    public static ArrayList<Rental> findByLibraryCardID(EntityManager em, int id){
        
        ArrayList<Rental> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Rental>) em.createQuery(
            "SELECT r FROM Rental r WHERE r.libraryCard.libraryCardID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Rental> findAll(EntityManager em){
        
        ArrayList<Rental> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Rental>) em.createQuery(
            "SELECT r FROM Rental r")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Rental> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Rental> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Rental>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Rental> findByDateQuery(EntityManager em, String q, Date date){
        ArrayList<Rental> result;
        
        System.out.println(date);
        
        em.getTransaction().begin();

        result = (ArrayList<Rental>) em.createQuery(q)
                .setParameter("parameter", date)
                .getResultList();

        em.getTransaction().commit();
        
        System.out.println(result);
        
        return result;
    }
    
    public static ArrayList<Rental> findByID(EntityManager em, int id){
        ArrayList<Rental> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Rental>) em.createQuery(
            "SELECT r FROM Rental r WHERE r.rentalID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
        
    }
}
