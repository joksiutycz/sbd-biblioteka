/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author jasiek
 */
@Entity
public class Genre {
    @Id
    @GeneratedValue
    private int genreID;
    
    private String name;
    
    @OneToMany(mappedBy = "genre")
    private List<Book> books;

    public int getGenreID() {
        return genreID;
    }

    public void setGenreID(int genreID) {
        this.genreID = genreID;
    }

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public static ArrayList<Genre> findAll(EntityManager em){
        
        ArrayList<Genre> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Genre>) em.createQuery(
            "SELECT g FROM Genre g")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Genre> findByID(EntityManager em, int id){
        ArrayList<Genre> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Genre>) em.createQuery(
            "SELECT g FROM Genre g WHERE g.genreID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Genre> findByName(EntityManager em, String name){
        
        ArrayList<Genre> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Genre>) em.createQuery(
            "SELECT g FROM Genre g WHERE UPPER(g.name) LIKE UPPER(:name)")
            .setParameter("name", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Genre> findByBooksCount(EntityManager em, int count){
        
        ArrayList<Genre> result;
        
        em.getTransaction().begin();
        
        //TODO

        result = (ArrayList<Genre>) em.createQuery(
            "SELECT g FROM Genre g")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
