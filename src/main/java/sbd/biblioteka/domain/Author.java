/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author jasiek
 */
@Entity
public class Author {
    @Id
    @GeneratedValue
    private int authorID;
    
    private String name;
    private String surname;
    
    @ManyToMany(mappedBy = "authors")
    private List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public int getAuthorID() {
        return authorID;
    }

    public void setAuthorID(int authorID) {
        this.authorID = authorID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    @Override
    public String toString(){
        return name + " " + surname;
    }
    
    public static ArrayList<Author> findByName(EntityManager em, String name){
        
        ArrayList<Author> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Author>) em.createQuery(
            "SELECT a FROM Author a WHERE a.name LIKE :custName")
            .setParameter("custName", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Author> findAll(EntityManager em){
        ArrayList<Author> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Author>) em.createQuery(
            "SELECT a FROM Author a")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Author> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Author> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Author>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Author> findByID(EntityManager em, int id){
        ArrayList<Author> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Author>) em.createQuery(
            "SELECT a FROM Author a WHERE a.authorID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
