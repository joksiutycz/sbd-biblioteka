/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Query;

/**
 *
 * @author jasiek
 */
@Entity
public class Client {
    @Id
    @GeneratedValue
    private int clientID;
    
    @OneToOne
    @JoinColumn(name = "addressID")
    private Address address;
    
    @Column
    private String name;
    
    @Column
    private String surname;
    
    @Column
    private String email;
    
    

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public static ArrayList<Client> findByName(EntityManager em, String name){
        
        ArrayList<Client> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Client>) em.createQuery(
            "SELECT c FROM Client c WHERE c.name LIKE :custName")
            .setParameter("custName", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Client> findAll(EntityManager em){
        ArrayList<Client> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Client>) em.createQuery(
            "SELECT c FROM Client c")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Client> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Client> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Client>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Client> findByID(EntityManager em, int id){
        ArrayList<Client> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Client>) em.createQuery(
            "SELECT c FROM Client c WHERE c.clientID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    @Override 
    public String toString(){
        return Integer.toString(clientID);
    }
    
    
}
