/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author jasiek

*/
@Entity
public class Position {
    @Id
    @GeneratedValue
    private int positionID;
    
    private String name;
    
    @OneToMany(mappedBy = "position")
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getPositionID() {
        return positionID;
    }

    public void setPositionID(int positionID) {
        this.positionID = positionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    public static ArrayList<Position> findAll(EntityManager em){
        
        ArrayList<Position> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Position>) em.createQuery(
            "SELECT p FROM Position p")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Position> findByID(EntityManager em, int id){
        ArrayList<Position> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Position>) em.createQuery(
            "SELECT p FROM Position p WHERE p.positionID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Position> findByName(EntityManager em, String name){
        
        ArrayList<Position> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Position>) em.createQuery(
            "SELECT p FROM Position p WHERE UPPER(p.name) LIKE UPPER(:name)")
            .setParameter("name", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Position> findByEmployeesCount(EntityManager em, int count){
        
        ArrayList<Position> result;
        
        em.getTransaction().begin();
        
        //TODO

        result = (ArrayList<Position>) em.createQuery(
            "SELECT p FROM Position p")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Position> findEmployees(EntityManager em, int id){
        
        ArrayList<Position> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Position>) em.createQuery(
                      "SELECT p "
                    + "FROM Position p "
                    + "JOIN p.employees emps "
                    + "WHERE emps.position.positionID = :id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
