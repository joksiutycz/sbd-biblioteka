/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Registry {
    @Id
    @GeneratedValue
    private int registryID;
    
    @OneToOne
    @JoinColumn(name = "libraryCardID")
    private LibraryCard libraryCard;
    
    @OneToOne
    @JoinColumn(name = "readingRoomID")
    private ReadingRoom readingRoom;
    
    private Timestamp enterDate;
    
    private Timestamp leftDate;

    public int getRegistryID() {
        return registryID;
    }

    public void setRegistryID(int registryID) {
        this.registryID = registryID;
    }

    public LibraryCard getLibraryCard() {
        return libraryCard;
    }

    public void setLibraryCard(LibraryCard libraryCard) {
        this.libraryCard = libraryCard;
    }

    public ReadingRoom getReadingRoom() {
        return readingRoom;
    }

    public void setReadingRoom(ReadingRoom readingRoom) {
        this.readingRoom = readingRoom;
    }

    public Timestamp getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(Timestamp enterDate) {
        this.enterDate = enterDate;
    }

    public Timestamp getLeftDate() {
        return leftDate;
    }

    public void setLeftDate(Timestamp leftDate) {
        this.leftDate = leftDate;
    }
    
    public static ArrayList<Registry> findAll(EntityManager em){
        
        ArrayList<Registry> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Registry>) em.createQuery(
            "SELECT r FROM Registry r")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Registry> findByID(EntityManager em, int id){
        
        ArrayList<Registry> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Registry>) em.createQuery(
            "SELECT r FROM Registry r WHERE r.registryID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Registry> findByLibraryCardID(EntityManager em, int libraryCardID){
        
        ArrayList<Registry> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Registry>) em.createQuery(
            "SELECT r FROM Registry r WHERE r.libraryCard.libraryCardID=:id")
            .setParameter("id", libraryCardID)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
   public static ArrayList<Registry> findByQuery(EntityManager em, String query, String param){
        
        ArrayList<Registry> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Registry>) em.createQuery(query)
            .setParameter("parameter", param)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
