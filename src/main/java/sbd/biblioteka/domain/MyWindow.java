/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import sbd.biblioteka.Biblioteka;

/**
 *
 * @author jasiek
 */

public abstract class MyWindow extends JFrame {
    
        JPanel panel, searchPanel, crudPanel;
        JButton addButton, editButton, deleteButton,
                searchButton;
        
        protected WindowListener windowListener;

        JTable table;
        protected AbstractTableModel atm;

        public MyWindow(AbstractTableModel _atm, WindowListener _wl, String title) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    
                    windowListener = _wl;

                    JFrame frame = new JFrame(title);

                    atm = _atm;
                    table = new JTable(atm);

                    frame.setSize(800, 600);

                    Toolkit tk = Toolkit.getDefaultToolkit();

                    Dimension dim = tk.getScreenSize();

                    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
                    int yPos = (dim.height / 2) - (frame.getHeight() / 2);

                    frame.setLocation(xPos, yPos);

                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    addButton = new JButton("Dodaj");
                    editButton = new JButton("Edytuj");
                    deleteButton = new JButton("Usuń");

                    ListenForButton lfb = new ListenForButton();

                    addButton.addActionListener(lfb);
                    editButton.addActionListener(lfb);
                    deleteButton.addActionListener(lfb);

                    panel = new JPanel();
                    panel.setLayout(new BorderLayout());
                    crudPanel = new JPanel();

                    panel.add(new JScrollPane(table));
                    crudPanel.add(addButton);
                    crudPanel.add(editButton);
                    crudPanel.add(deleteButton);

                    frame.add(panel, BorderLayout.CENTER);
                    frame.add(crudPanel, BorderLayout.PAGE_START);

                    frame.setVisible(true);

                    frame.addWindowListener(windowListener);
                }
            });

        }

        private class ListenForButton implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == addButton) {
                    add();

                } else if (e.getSource() == editButton) {
                    if(table.getSelectedRowCount() == 1)
                        edit(table.getSelectedRow());

                } else if (e.getSource() == deleteButton) {
                    delete(table.getSelectedRows());
                }
            }
        }
        
        protected abstract void add();
        
        protected abstract void edit(int idx);
       
        protected abstract void delete(int [] idxs);     
}
