/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */
@Entity
public class Book {
    @Id
    @GeneratedValue
    private int bookID;
    
    @ManyToOne
    @JoinColumn(name = "genreID")
    private Genre genre;
    
    @ManyToOne
    @JoinColumn(name = "branchID")
    private Branch branch;
    
    @ManyToMany
    @JoinTable(
            name = "Book_authors",
            joinColumns = {@JoinColumn(name = "bookID")},
            inverseJoinColumns = {@JoinColumn(name = "authorID")}   
            )
    private List<Author> authors;
    

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
    
    private String name;

    private int releaseYear;
    
    private int count;
    
    private int rentedCount;

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getRentedCount() {
        return rentedCount;
    }

    public void setRentedCount(int rentedCount) {
        this.rentedCount = rentedCount;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    
    public static ArrayList<Book> findByName(EntityManager em, String name){
        
        ArrayList<Book> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk WHERE bk.name LIKE :custName")
            .setParameter("custName", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findAll(EntityManager em){
        ArrayList<Book> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Book> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByID(EntityManager em, int id){
        ArrayList<Book> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk WHERE bk.bookID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByReleaseYear(EntityManager em, int releaseYear){
        ArrayList<Book> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk WHERE bk.releaseYear=:releaseYear")
            .setParameter("releaseYear", releaseYear)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByCount(EntityManager em, int count){
        ArrayList<Book> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk WHERE bk.count=:count")
            .setParameter("count", count)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByRentedCount(EntityManager em, int rentedCount){
        ArrayList<Book> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Book>) em.createQuery(
            "SELECT bk FROM Book bk WHERE bk.rentedCount:rentedCount")
            .setParameter("rentedCount", rentedCount)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Book> findByAuthor(EntityManager em, String author){
        ArrayList<Book> result;
        
        em.getTransaction().begin();
        

        result = (ArrayList<Book>) em.createQuery(
                      "SELECT bk "
                    + "FROM Book bk "
                    + "JOIN bk.authors aths "
                    + "WHERE CONCAT(UPPER(aths.name), ' ', UPPER(aths.surname)) "
                    + "LIKE UPPER(:author)")
            .setParameter("author", author)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
