/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sbd.biblioteka.domain;

import java.sql.Date;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author jasiek
 */
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private int employeeID;
    
    private Date hireDate;
    
    private String name;
    
    private String surnameString;
    
    @ManyToOne
    @JoinColumn(name = "positionID")
    private Position position;
    
    @ManyToOne
    @JoinColumn(name = "branchID")
    private Branch branch;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnameString() {
        return surnameString;
    }

    public void setSurnameString(String surnameString) {
        this.surnameString = surnameString;
    }
    
    public static ArrayList<Employee> findByName(EntityManager em, String name){
        
        ArrayList<Employee> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Employee>) em.createQuery(
            "SELECT e FROM Employee e WHERE e.name LIKE :custName")
            .setParameter("custName", name)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Employee> findAll(EntityManager em){
        ArrayList<Employee> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Employee>) em.createQuery(
            "SELECT e FROM Employee e")
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Employee> findByQuery(EntityManager em, String q, String parameter){
        ArrayList<Employee> result;
        
        em.getTransaction().begin();

        result = (ArrayList<Employee>) em.createQuery(q)
                .setParameter("parameter", parameter)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
    public static ArrayList<Employee> findByID(EntityManager em, int id){
        ArrayList<Employee> result;
        
        
        em.getTransaction().begin();

        result = (ArrayList<Employee>) em.createQuery(
            "SELECT e FROM Employee e WHERE e.employeeID=:id")
            .setParameter("id", id)
            .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
    
     public static ArrayList<Employee> findByDateQuery(EntityManager em, String q, Date date){
        ArrayList<Employee> result;
   
        em.getTransaction().begin();

        result = (ArrayList<Employee>) em.createQuery(q)
                .setParameter("parameter", date)
                .getResultList();

        em.getTransaction().commit();
        
        return result;
    }
}
