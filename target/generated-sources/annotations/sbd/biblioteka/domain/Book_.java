package sbd.biblioteka.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ {

	public static volatile SingularAttribute<Book, Genre> genre;
	public static volatile SingularAttribute<Book, String> name;
	public static volatile SingularAttribute<Book, Integer> count;
	public static volatile SingularAttribute<Book, Integer> rentedCount;
	public static volatile SingularAttribute<Book, Branch> branch;
	public static volatile SingularAttribute<Book, Integer> releaseYear;
	public static volatile SingularAttribute<Book, Integer> bookID;
	public static volatile ListAttribute<Book, Author> authors;

}

