package sbd.biblioteka.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReadingRoom.class)
public abstract class ReadingRoom_ {

	public static volatile SingularAttribute<ReadingRoom, Integer> slots;
	public static volatile SingularAttribute<ReadingRoom, Integer> freeSlots;
	public static volatile SingularAttribute<ReadingRoom, Integer> readingRoomID;
	public static volatile SingularAttribute<ReadingRoom, Branch> branch;

}

