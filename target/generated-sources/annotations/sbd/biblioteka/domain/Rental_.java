package sbd.biblioteka.domain;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rental.class)
public abstract class Rental_ {

	public static volatile SingularAttribute<Rental, Date> rentalDate;
	public static volatile SingularAttribute<Rental, Date> returnDate;
	public static volatile SingularAttribute<Rental, Book> book;
	public static volatile SingularAttribute<Rental, Date> maxReturnDate;
	public static volatile SingularAttribute<Rental, LibraryCard> libraryCard;
	public static volatile SingularAttribute<Rental, Integer> rentalID;

}

