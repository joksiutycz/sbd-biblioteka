package sbd.biblioteka.domain;

import java.sql.Time;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Library.class)
public abstract class Library_ {

	public static volatile SingularAttribute<Library, Integer> libraryID;
	public static volatile SingularAttribute<Library, Time> closeTime;
	public static volatile SingularAttribute<Library, Time> openTime;
	public static volatile ListAttribute<Library, Branch> branches;

}

