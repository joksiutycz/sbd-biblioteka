package sbd.biblioteka.domain;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Employee.class)
public abstract class Employee_ {

	public static volatile SingularAttribute<Employee, Date> hireDate;
	public static volatile SingularAttribute<Employee, String> name;
	public static volatile SingularAttribute<Employee, Integer> employeeID;
	public static volatile SingularAttribute<Employee, Position> position;
	public static volatile SingularAttribute<Employee, String> surnameString;
	public static volatile SingularAttribute<Employee, Branch> branch;

}

