package sbd.biblioteka.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Branch.class)
public abstract class Branch_ {

	public static volatile SingularAttribute<Branch, Integer> branchID;
	public static volatile SingularAttribute<Branch, Address> address;
	public static volatile SingularAttribute<Branch, Library> library;
	public static volatile ListAttribute<Branch, Book> books;
	public static volatile ListAttribute<Branch, Employee> employees;

}

