package sbd.biblioteka.domain;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LibraryCard.class)
public abstract class LibraryCard_ {

	public static volatile SingularAttribute<LibraryCard, Client> client;
	public static volatile SingularAttribute<LibraryCard, String> theme;
	public static volatile ListAttribute<LibraryCard, Rental> rentals;
	public static volatile SingularAttribute<LibraryCard, Date> creationDate;
	public static volatile SingularAttribute<LibraryCard, Integer> libraryCardID;

}

