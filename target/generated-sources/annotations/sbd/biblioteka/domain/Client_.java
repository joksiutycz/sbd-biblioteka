package sbd.biblioteka.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SingularAttribute<Client, Integer> clientID;
	public static volatile SingularAttribute<Client, Address> address;
	public static volatile SingularAttribute<Client, String> surname;
	public static volatile SingularAttribute<Client, String> name;
	public static volatile SingularAttribute<Client, String> email;

}

