package sbd.biblioteka.domain;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Registry.class)
public abstract class Registry_ {

	public static volatile SingularAttribute<Registry, Timestamp> leftDate;
	public static volatile SingularAttribute<Registry, Timestamp> enterDate;
	public static volatile SingularAttribute<Registry, Integer> registryID;
	public static volatile SingularAttribute<Registry, LibraryCard> libraryCard;
	public static volatile SingularAttribute<Registry, ReadingRoom> readingRoom;

}

